pipeline: Simul_Gpop
params:
  results_dir: /Results
  coal_simul_step: simul_coal
  simul_coal_output_dir: simul_coal
  simul_coal_command: ''
  simul_coal_pop1: ''
  simul_coal_pop2: ''
  simul_coal_outgroup: ''
  simul_coal_samp1: 10
  simul_coal_samp2: 8
  simul_coal_nlocus: 1000
  simul_coal_mlength: 1000
  simul_coal_sdlength: 10
  simul_coal_Nref: 100000
  simul_coal_mu: '0.00000002'
  simul_coal_seed: 0
  simul_coal_model: SC
  simul_coal_nreps: 1
  simul_coal_spec_model: island
  simul_coal_mig: homo
  simul_coal_dir: asym
  simul_coal_n1: 1
  simul_coal_n2: 2
  simul_coal_na: 3
  simul_coal_tau: 6
  simul_coal_tausmall: 0
  simul_coal_nm1: 2
  simul_coal_nm2: 0.2
  simul_coal_prop: 0
  estimate_model_params_dadi: dadi
  dadi_output_dir: dadi
  dadi_command: ''
  dadi_masked: false
  dadi_folded: false
  dadi_projected: false
  dadi_datatype: simul
  dadi_proj_sample: '10,10 '
  dadi_grid_points: '10,20,30 '
  dadi_model_list: SI
  dadi_maxiterGlobal: 100
  dadi_accept: 1
  dadi_visit: 1.01
  dadi_Tini: 50
  dadi_no_local_search: false
  dadi_local_method: L-BFGS-B
  dadi_maxiterLocal: 20
  estimate_model_params_moments: moments
  moments_output_dir: moments
  moments_command: ''
  moments_masked: false
  moments_folded: false
  moments_projected: false
  moments_datatype: simul
  moments_proj_sample: '10,10 '
  moments_model_list: SI
  moments_maxiterGlobal: 100
  moments_accept: 1
  moments_visit: 1.01
  moments_Tini: 50
  moments_no_local_search: false
  moments_local_method: L-BFGS-B
  moments_maxiterLocal: 20
samples: []
groups: []
steps:
- title: Coalescent simulation
  name: coal_simul_step
  tools:
  - simul_coal
  default: simul_coal
- title: Estimate model parameters dadi
  name: estimate_model_params_dadi
  tools:
  - dadi
  default: dadi
- title: Estimate model parameters moments
  name: estimate_model_params_moments
  tools:
  - moments
  default: moments
params_info:
  results_dir:
    type: output_dir
  simul_coal_pop1:
    tool: simul_coal
    rule: simul_coal
    type: text
    label: Population 1 name
  simul_coal_pop2:
    tool: simul_coal
    rule: simul_coal
    type: text
    label: Population 2 name
  simul_coal_outgroup:
    tool: simul_coal
    rule: simul_coal
    type: text
    label: Outgroup name
  simul_coal_samp1:
    tool: simul_coal
    rule: simul_coal
    type: numeric
    label: Sample size of population 1
  simul_coal_samp2:
    tool: simul_coal
    rule: simul_coal
    type: numeric
    label: Sample size of population 2
  simul_coal_nlocus:
    tool: simul_coal
    rule: simul_coal
    type: numeric
    label: Number of loci
  simul_coal_mlength:
    tool: simul_coal
    rule: simul_coal
    type: numeric
    label: 'Contig length: mean'
  simul_coal_sdlength:
    tool: simul_coal
    rule: simul_coal
    type: numeric
    label: 'Contig length: standard deviation'
  simul_coal_Nref:
    tool: simul_coal
    rule: simul_coal
    type: numeric
    label: Size of the reference population
  simul_coal_mu:
    tool: simul_coal
    rule: simul_coal
    type: text
    label: Mutation rate per bp per generation
  simul_coal_seed:
    tool: simul_coal
    rule: simul_coal
    type: numeric
    label: 'Seed for random number generation : ! to be changed between replicates
      !'
  simul_coal_model:
    tool: simul_coal
    rule: simul_coal
    type: select
    label: Model
  simul_coal_nreps:
    tool: simul_coal
    rule: simul_coal
    type: numeric
    label: 'nreps: total number of multilocus sims'
  simul_coal_spec_model:
    tool: simul_coal
    rule: simul_coal
    type: select
    label: speciation model
  simul_coal_mig:
    tool: simul_coal
    rule: simul_coal
    type: select
    label: homo for homogenous migration, hetero for heterogeneous migration
  simul_coal_dir:
    tool: simul_coal
    rule: simul_coal
    type: select
    label: symetrical or asymetrical direction of migration
  simul_coal_n1:
    tool: simul_coal
    rule: simul_coal
    type: numeric
    label: ratio of N1/N0
  simul_coal_n2:
    tool: simul_coal
    rule: simul_coal
    type: numeric
    label: ratio of N2/N0
  simul_coal_na:
    tool: simul_coal
    rule: simul_coal
    type: numeric
    label: ratio of NA/N0
  simul_coal_tau:
    tool: simul_coal
    rule: simul_coal
    type: numeric
    label: divergence time
  simul_coal_tausmall:
    tool: simul_coal
    rule: simul_coal
    type: numeric
    label: secondary contact or ancient migration time
  simul_coal_nm1:
    tool: simul_coal
    rule: simul_coal
    type: numeric
    label: 4N1m1 migration into 1 from 2
  simul_coal_nm2:
    tool: simul_coal
    rule: simul_coal
    type: numeric
    label: 4N2m2 migration into 2 from 1
  simul_coal_prop:
    tool: simul_coal
    rule: simul_coal
    type: numeric
    label: Proportion of loci crossing the species barriers
  dadi_masked:
    tool: dadi
    rule: dadi
    type: checkbox
    label: '--masked : If set, mask the singletons in the sfs: [1,0] and [0,1].'
  dadi_folded:
    tool: dadi
    rule: dadi
    type: checkbox
    label: '--folded : If set, the sfs is folded (i.e. no outgroup to polarize the
      data).'
  dadi_projected:
    tool: dadi
    rule: dadi
    type: checkbox
    label: '--projected : If set, the sfs is projected down to n1,n2 (values to be
      set with option -n). If your data have missing calls for some individuals, projecting
      down to a smaller sample size will increase the number of SNPs you can use.
      It will average over all possible re-samplings of the larger sample size data
      (assuming random mating).'
  dadi_datatype:
    tool: dadi
    rule: dadi
    type: select
    label: 'Type of input: 1) simul: ms simulation. 2) sfs: sfs data. 3) snp-dadi:
      snp data with dadi format; the sample size is required (values to be set with
      options -j and -n). 4) vcf: vcf data; it requires -c to be provided and it will
      only include sites without missing data.'
  dadi_proj_sample:
    tool: dadi
    rule: dadi
    type: text
    label: '--proj_sample : Sample size of population 1 and 2 in the sfs. Recall that
      for a diploid organism we get two samples from each individual. These values
      are ignored if -j is not set.'
  dadi_grid_points:
    tool: dadi
    rule: dadi
    type: text
    label: '--grid_points : Size of the grids for extrapolation. dadi solves a partial
      differential equation, approximating the solution using a grid of points in
      population frequency space (diffusion approximation). The grid takes 3 numbers
      separated by a coma. Good results are obtained by setting the smallest grid
      size slightly larger than the largest sample size, but this can be altered depending
      on usage. If you are fitting a complex model, it may speed up the analysis considerably
      to first run an optimization at small grid sizes. They can then be refined by
      running another optimization with a finer grid.'
  dadi_model_list:
    tool: dadi
    rule: dadi
    type: select
    label: '--model_list : List the models to include in the inference. All available
      models are defined in dadi_models_2pop.py.'
  dadi_maxiterGlobal:
    tool: dadi
    rule: dadi
    type: numeric
    label: maximum global search iterations - in each iteration, it explores twice
      the number of parameters.
  dadi_accept:
    tool: dadi
    rule: dadi
    type: numeric
    label: parameter for acceptance distribution (lower values makes the probability
      of acceptance smaller)
  dadi_visit:
    tool: dadi
    rule: dadi
    type: numeric
    label: parameter for visiting distribution (higher values makes the algorithm
      jump to a more distant region)
  dadi_Tini:
    tool: dadi
    rule: dadi
    type: numeric
    label: initial temperature
  dadi_no_local_search:
    tool: dadi
    rule: dadi
    type: checkbox
    label: If set to True, a Generalized Simulated Annealing will be performed with
      no local search strategy applied
  dadi_local_method:
    tool: dadi
    rule: dadi
    type: text
    label: local search method
  dadi_maxiterLocal:
    tool: dadi
    rule: dadi
    type: numeric
    label: maxiterLocal
  moments_masked:
    tool: moments
    rule: moments
    type: checkbox
    label: '--masked : If set, mask the singletons in the sfs: [1,0] and [0,1].'
  moments_folded:
    tool: moments
    rule: moments
    type: checkbox
    label: '--folded : If set, the sfs is folded (i.e. no outgroup to polarize the
      data).'
  moments_projected:
    tool: moments
    rule: moments
    type: checkbox
    label: '--projected : If set, the sfs is projected down to n1,n2 (values to be
      set with option -n). If your data have missing calls for some individuals, projecting
      down to a smaller sample size will increase the number of SNPs you can use.
      It will average over all possible re-samplings of the larger sample size data
      (assuming random mating).'
  moments_datatype:
    tool: moments
    rule: moments
    type: select
    label: 'Type of input: 1) simul: ms simulation. 2) sfs: sfs data. 3) snp-moments:
      snp data with moments format; the sample size is required (values to be set
      with options -j and -n). 4) vcf: vcf data; it requires -c to be provided and
      it will only include sites without missing data.'
  moments_proj_sample:
    tool: moments
    rule: moments
    type: text
    label: '--proj_sample : Sample size of population 1 and 2 in the sfs. Recall that
      for a diploid organism we get two samples from each individual. These values
      are ignored if -j is not set.'
  moments_model_list:
    tool: moments
    rule: moments
    type: select
    label: '--model_list : List the models to include in the inference. All available
      models are defined in moments_models_2pop.py.'
  moments_maxiterGlobal:
    tool: moments
    rule: moments
    type: numeric
    label: maximum global search iterations - in each iteration, it explores twice
      the number of parameters.
  moments_accept:
    tool: moments
    rule: moments
    type: numeric
    label: parameter for acceptance distribution (lower values makes the probability
      of acceptance smaller)
  moments_visit:
    tool: moments
    rule: moments
    type: numeric
    label: parameter for visiting distribution (higher values makes the algorithm
      jump to a more distant region)
  moments_Tini:
    tool: moments
    rule: moments
    type: numeric
    label: initial temperature
  moments_no_local_search:
    tool: moments
    rule: moments
    type: checkbox
    label: If set to True, a Generalized Simulated Annealing will be performed with
      no local search strategy applied
  moments_local_method:
    tool: moments
    rule: moments
    type: text
    label: local search method
  moments_maxiterLocal:
    tool: moments
    rule: moments
    type: numeric
    label: maxiterLocal
prepare_report_scripts:
- dadi.prepare.report.py
- moments.prepare.report.py
prepare_report_outputs:
  dadi:
  - dadi-full_mqc.txt
  moments:
  - moments-full_mqc.txt
outputs:
  simul_coal:
    simul_coal:
    - name: msout
      file: msout
      description: msout
  dadi:
    dadi:
    - name: full
      file: dadi-full.txt
      description: ''
    - name: easy
      file: dadi-easy.txt
      description: ''
    - name: modelsfs
      file: dadi-modelsfs.txt
      description: ''
    - name: img
      file: dadi_{model}_mqc.png
      description: ''
  moments:
    moments:
    - name: full
      file: moments-full.txt
      description: ''
    - name: easy
      file: moments-easy.txt
      description: ''
    - name: modelsfs
      file: moments-modelsfs.txt
      description: ''
    - name: img
      file: moments_{model}_mqc.png
      description: ''
multiqc:
  simul_coal: custom
  dadi: custom
  moments: custom
