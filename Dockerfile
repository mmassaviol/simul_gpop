FROM mbbteam/mbb_workflows_base:latest as alltools

COPY sources/simul_coal_sources /sources/simul_coal_sources


RUN pip3 install numpy

RUN pip3 install biopython==1.76

COPY sources/dadi_sources /sources/dadi_sources


RUN pip3 install scipy

RUN pip3 install matplotlib

RUN cd /sources/dadi_sources/dadi-wrapper/dadi-2.0.6_modif \
 && python3 setup.py install

COPY sources/moments_sources /sources/moments_sources


RUN pip3 install Cython

RUN pip3 install mpmath

RUN cd /sources/moments_sources/moments-wrapper/moments-1.0.0_modif \
 && python3 setup.py install



EXPOSE 3838
CMD ["Rscript", "-e", "setwd('/sagApp/'); shiny::runApp('/sagApp/app.R',port=3838 , host='0.0.0.0')"]


FROM alltools

COPY files /workflow
COPY sagApp /sagApp

