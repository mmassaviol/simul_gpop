This is an ongoing project to wrap tools and models developped by Camille Roux and Christelle Fraisse.
More details about the models in http://onlinelibrary.wiley.com/doi/10.1111/jeb.12425/abstract

The coalescent simulator used here is msnsam (Ross-Ibarra et al, 2008).
The demographic inference uses dadi (https://dadi.readthedocs.io/en/latest/) or moments (https://bitbucket.org/simongravel/moments/)


