#!/bin/bash

###############################################################################
# To run: ./ms_to_dadi.sh SI
###############################################################################
# Model to pick from:
#	SI: Strict Isolation
#	IM: Isolation with Migration
#	SC: Secondary Contact
#	AM: Ancient Migration
#	IM2M, SC2M, AM2M: heterogeneity of migration rates along the genome (binomial: m=0 vs m)
#
# Demographic parameters (IM):
#	samp1, samp2 = sample size of population 1 and 2.
#	nlocus = number of unkinked loci.
#	L = locus length.
#	mu = mutation rate per bp per gen.
#	Nref = size of the reference population  [arbitrarily fixed].
#	theta = 4 * Nref * mu * L = pop. diversity (but in DaDi/Moments: theta is optimized, and so Nref is deduced from the formula).
#	rho = 4 * Nref * r = pop. recombination rate within locus [fixed at theta/2].
#	N1, N2, Na = population sizes in units Nref individuals. 
#		     Convert with N1 = n1_estim * Nref.
#	Ts = time of split in units 4*Nref generations (but 2*Nref generations in DaDi/Moments). 
#	     Convert with Ts = ts_estim * 4*Nref.
#	m12, m21 = migration rates in units 4*Nref*M12 (but 2*Nref*M12 in DaDi/Moments), where M12=M2to1 is the fraction of pop1 that comes from pop2 per gen.
#		   Convert with M12 = m_estim / (4*Nref).
#
# 	run: msnsam samp1+samp2 nlocus -t theta -r rho L -I 2 samp1 samp2 0 -n 1 N1 -n 2 N2 -ej Ts 2 1 -en Ts 1 Na -m 1 2 m12 -m 2 1 m21
###############################################################################
######################################################################################################################################################
#							SIMULATION SETTINGS
######################################################################################################################################################
# Path to sources
toProg=/sources/simul_coal_sources/
# Describes the genetic data (length, sample size pop 1, sample size pop 2, theta, rho)
bpfile="bpfile"
# Describes the speciation model
argfile=`echo argfile_$1`
# Name of population 1, population 2, outgroup
pop1="pop1"
pop2="pop2"
outg="outgroup"
# Sample size of population 1
samp1=10
# Sample size of population 2
samp2=8
# Number of loci
nlocus=1000
# Contig length: mean
mlength=1000
# Contig length: standard deviation
sdlength=10
# Size of the reference population
Nref=100000
# Mutation rate per bp per generation
mu=0.00000002
# Seed for random number generation *** to be changed between replicates ***
seed=$3
# Working directory
wd=$2
mkdir -p ${wd}/${1}_${seed}
cd ${wd}/${1}_${seed}


######################################################################################################################################################
#							SIMULATE UNDER A SPECIFIC MODEL
######################################################################################################################################################
#################### Generate the "bpfile"
##########################################
myNames=`echo ${pop1} ${pop2} ${outg}`
python3 ${toProg}/tobpfile.py -c "${myNames}" -l ${samp1} -L ${samp2} -N ${Nref} -M ${mu} -n ${nlocus} -m ${mlength} -d ${sdlength}

###########################################
#################### Strict Isolation model
###########################################
if [ "$1" = "SI" ]; then

	##### Generate the "argfile"
	echo  "nreps = 1
	model = iso 
	mig = hetero 
	dir = asym 
	n1 = 1 
	n1 = 1 
	n2 = 2 
	n2 = 2 
	na = 3 
	na = 3 
	tau = 6 
	tau = 6 
	tausmall = 0 
	tausmall = 0 
	nm1 = 0 
	nm1 = 0 
	nm2 = 0 
	nm2 = 0 
	prop = 0 
	prop = 0 " > ${wd}/${1}_${seed}/argfile_${1}
		
	##### Do the coalescent simulation
	${toProg}priorgen_linux --bpfile ${bpfile} --arg-file ${argfile} --prior prior --seed ${seed} | ${toProg}/msnsam_linux tbs ${nlocus} -t tbs -r tbs tbs -I 2 tbs tbs 0 -m 1 2 tbs -m 2 1 tbs -n 1 tbs -n 2 tbs -ej tbs 2 1 -eN tbs tbs >msout

	##### Modify msout so DaDi can extract the sample sizes
	sed -i "s/-I 2 tbs tbs/-I 2 ${samp1} ${samp2}/g" msout
fi

###################################################
#################### Isolation with Migration model
###################################################
if [ "$1" = "IM" ]; then

	##### Generate the "argfile"
	echo "nreps = 1 
	model = island 
	mig = homo 
	dir = asym 
	n1 = 1 
	n1 = 1 
	n2 = 2 
	n2 = 2 
	na = 3 
	na = 3 
	tau = 6 
	tau = 6 
	tausmall = 0 
	tausmall = 0 
	nm1 = 2 
	nm1 = 2 
	nm2 = 0.2 
	nm2 = 0.2 
	prop = 0 
	prop = 0" > ${wd}/${1}_${seed}/argfile_${1}
		
	##### Do the coalescent simulation
	${toProg}priorgen_linux --bpfile ${bpfile} --arg-file ${argfile} --prior prior --seed ${seed} | ${toProg}/msnsam_linux tbs ${nlocus} -t tbs -r tbs tbs -I 2 tbs tbs 0 -m 1 2 tbs -m 2 1 tbs -n 1 tbs -n 2 tbs -ej tbs 2 1 -eN tbs tbs >msout

	##### Modify msout so DaDi can extract the sample sizes
	sed -i "s/-I 2 tbs tbs/-I 2 ${samp1} ${samp2}/g" msout
fi


if [ "$1" = "IM2M" ]; then

	##### Generate the "argfile"
	echo "nreps = 1
	model = island 
	mig = hetero 
	dir = asym 
	n1 = 1 
	n1 = 1 
	n2 = 2 
	n2 = 2 
	na = 3 
	na = 3 
	tau = 6 
	tau = 6 
	tausmall = 0 
	tausmall = 0 
	nm1 = 2 
	nm1 = 2 
	nm2 = 0.2 
	nm2 = 0.2 
	prop = 0.1 
	prop = 0.1 " > ${wd}/${1}_${seed}/argfile_${1}
		
	##### Do the coalescent simulation
	${toProg}priorgen_linux --bpfile ${bpfile} --arg-file ${argfile} --prior prior --seed ${seed} | ${toProg}/msnsam_linux tbs ${nlocus} -t tbs -r tbs tbs -I 2 tbs tbs 0 -m 1 2 tbs -m 2 1 tbs -n 1 tbs -n 2 tbs -ej tbs 2 1 -eN tbs tbs >msout

	##### Modify msout so DaDi can extract the sample sizes
	sed -i "s/-I 2 tbs tbs/-I 2 ${samp1} ${samp2}/g" msout
fi


############################################
#################### Secondary Contact model
############################################
if [ "$1" = "SC" ]; then

	##### Generate the "argfile"
	echo "nreps = 1
	model = allo 
	mig = homo 
	dir = asym 
	n1 = 1 
	n1 = 1 
	n2 = 2 
	n2 = 2 
	na = 3 
	na = 3 
	tau = 5 
	tau = 5 
	tausmall = 1 
	tausmall = 1 
	nm1 = 2 
	nm1 = 2 
	nm2 = 0.2 
	nm2 = 0.2 
	prop = 0 
	prop = 0 " > ${wd}/${1}_${seed}/argfile_${1}
		
	##### Do the coalescent simulation
	${toProg}/priorgen_linux --bpfile ${bpfile} --arg-file ${argfile} --prior prior --seed ${seed} | ${toProg}/msnsam_linux tbs ${nlocus} -t tbs -r tbs tbs -I 2 tbs tbs 0 -m 1 2 tbs -m 2 1 tbs -n 1 tbs -n 2 tbs -eM tbs 0 -ej tbs 2 1 -eN tbs tbs >msout

	##### Modify msout so DaDi can extract the sample sizes
	sed -i "s/-I 2 tbs tbs/-I 2 ${samp1} ${samp2}/g" msout
fi


if [ "$1" = "SC2M" ]; then
	
	##### Generate the "argfile"
	echo "nreps = 1
	model = allo 
	mig = hetero 
	dir = asym 
	n1 = 1 
	n1 = 1 
	n2 = 2 
	n2 = 2 
	na = 3 
	na = 3 
	tau = 5 
	tau = 5 
	tausmall = 1 
	tausmall = 1 
	nm1 = 2 
	nm1 = 2 
	nm2 = 0.2 
	nm2 = 0.2 
	prop = 0.1 
	prop = 0.1 " > ${wd}/${1}_${seed}/argfile_${1}
	
	##### Do the coalescent simulation
	${toProg}/priorgen_linux --bpfile ${bpfile} --arg-file ${argfile} --prior prior --seed ${seed} | ${toProg}/msnsam_linux tbs ${nlocus} -t tbs -r tbs tbs -I 2 tbs tbs 0 -m 1 2 tbs -m 2 1 tbs -n 1 tbs -n 2 tbs -eM tbs 0 -ej tbs 2 1 -eN tbs tbs >msout

	##### Modify msout so DaDi can extract the sample sizes
	sed -i "s/-I 2 tbs tbs/-I 2 ${samp1} ${samp2}/g" msout
fi

############################################
#################### Ancient Migration model
############################################
if [ "$1" = "AM" ]; then
	
	##### Generate the "argfile"
	echo "nreps = 1
	model = sym 
	mig = homo 
	dir = asym 
	n1 = 1 
	n1 = 1 
	n2 = 2 
	n2 = 2 
	na = 3 
	na = 3 
	tau = 5 
	tau = 5 
	tausmall = 1 
	tausmall = 1 
	nm1 = 2 
	nm1 = 2 
	nm2 = 0.2 
	nm2 = 0.2 
	prop = 0 
	prop = 0 " > ${wd}/${1}_${seed}/argfile_${1}
		
	##### Do the coalescent simulation
	${toProg}/priorgen_linux --bpfile ${bpfile} --arg-file ${argfile} --prior prior --seed ${seed} | ${toProg}/msnsam_linux tbs ${nlocus} -t tbs -r tbs tbs -I 2 tbs tbs 0 -m 1 2 0 -m 2 1 0 -n 1 tbs -n 2 tbs -ema tbs 2 0 tbs tbs 0 -ej tbs 2 1 -eN tbs tbs >msout

	##### Modify msout so DaDi can extract the sample sizes
	sed -i "s/-I 2 tbs tbs/-I 2 ${samp1} ${samp2}/g" msout
fi


if [ "$1" = "AM2M" ]; then
	
	##### Generate the "argfile"
	echo "nreps = 1
	model = sym 
	mig = hetero 
	dir = asym 
	n1 = 1 
	n1 = 1 
	n2 = 2 
	n2 = 2 
	na = 3 
	na = 3 
	tau = 5 
	tau = 5 
	tausmall = 1 
	tausmall = 1 
	nm1 = 2 
	nm1 = 2 
	nm2 = 0.2 
	nm2 = 0.2 
	prop = 0.1 
	prop = 0.1 " > ${wd}/${1}_${seed}/argfile_${1}
		
	##### Do the coalescent simulation
	${toProg}/priorgen_linux --bpfile ${bpfile} --arg-file ${argfile} --prior prior --seed ${seed} | ${toProg}/msnsam_linux tbs ${nlocus} -t tbs -r tbs tbs -I 2 tbs tbs 0 -m 1 2 0 -m 2 1 0 -n 1 tbs -n 2 tbs -ema tbs 2 0 tbs tbs 0 -ej tbs 2 1 -eN tbs tbs >msout

	##### Modify msout so DaDi can extract the sample sizes
	sed -i "s/-I 2 tbs tbs/-I 2 ${samp1} ${samp2}/g" msout
fi
