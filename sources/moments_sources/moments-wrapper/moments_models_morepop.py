#!/usr/local/bin/python
# -*- coding: utf-8 -*-

######################################################################################################################################################
#								IMPORT PACKAGES
######################################################################################################################################################
import sys
import moments
from moments import Misc,Spectrum,Numerics,Manips,Integration,Demographics1D,Demographics2D
import numpy
######################################################################################################################################################
#								3 POPULATIONS
######################################################################################################################################################
def P3_SI_SI(params, ns):
    nu1, nu23, nu2, nu3, Ts1_23, Ts2_3, O = params
    n1, n2, n3 = ns
    """
    Three populations: (pop1;(pop2;pop3)).
    Strict Isolation during the two splits.

    nu1: Size of population 1 after first split and until present.
    nu23: Size of ancestral population to (2,3) after first split.
    nu2: Size of population 2 after second split and until present.
    nu3: Size of population 3 after second split and until present.
    Ts1_23: Duration of divergence in isolation - first split.
    Ts2_3: Duration of divergence in isolation - second split.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Equilibrium ancestral population
    sts = moments.LinearSystem_1D.steady_state_1D(n1+n2+n3)
    phi = moments.Spectrum(sts)
    # Split event between 1 and (2,3)
    phi = moments.Manips.split_1D_to_2D(phi, n1, n2+n3)
    phi.integrate([nu1, nu23], Ts1_23)
    # Split event between 2 and 3
    phi = moments.Manips.split_2D_to_3D_2(phi, n2, n3)
    phi.integrate([nu1, nu2, nu3], Ts2_3)

    # Calculate the well-oriented spectrum
    fsO = phi
    # Calculate the mis-oriented spectrum
    fsM = moments.Numerics.reverse_array(fsO)

    # Sum all spectra
    fs = O*fsO + (1-O)*fsM
    return fs


def P3_IM_IM(params, ns):
    nu1, nu23, nu2, nu3, m1_23, m23_1, m1_2, m2_1, m1_3, m3_1, m2_3, m3_2, Ts1_23, Ts2_3, O = params
    n1, n2, n3 = ns
    """
    Three populations: (pop1;(pop2;pop3)).
    Isolation with Migration during the two splits.

    nu1: Size of population 1 after first split and until present.
    nu23: Size of ancestral population to (2,3) after first split.
    nu2: Size of population 2 after second split and until present.
    nu3: Size of population 3 after second split and until present.
    mi_j: Migration from pop j to pop i.
    Ts1_23: Duration of divergence with migration - first split.
    Ts2_3: Duration of divergence with migration - second split.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    sts = moments.LinearSystem_1D.steady_state_1D(n1+n2+n3)
    phi = moments.Spectrum(sts)
    phi = moments.Manips.split_1D_to_2D(phi, n1, n2+n3)
    phi.integrate([nu1, nu23], Ts1_23, m=numpy.array([[0, m1_23],[m23_1, 0]]))
    phi = moments.Manips.split_2D_to_3D_2(phi, n2, n3)
    phi.integrate([nu1, nu2, nu3], Ts2_3, m=numpy.array([[0, m1_2, m1_3],[m2_1, 0, m2_3],[m3_1, m3_2, 0]]))
    fsO = phi
    fsM = moments.Numerics.reverse_array(fsO)

    # Sum all spectra
    fs = O*fsO + (1-O)*fsM
    return fs


def P3_SI_IM(params, ns):
    nu1, nu23, nu2, nu3, m1_2, m2_1, m1_3, m3_1, m2_3, m3_2, Ts1_23, Ts2_3, O = params
    n1, n2, n3 = ns
    """
    Three populations: (pop1;(pop2;pop3)).
    Strict Isolation during the first split, Isolation with Migration during the second split.

    nu1: Size of population 1 after first split and until present.
    nu23: Size of ancestral population to (2,3) after first split.
    nu2: Size of population 2 after second split and until present.
    nu3: Size of population 3 after second split and until present.
    mi_j: Migration from pop j to pop i.
    Ts1_23: Duration of divergence in isolation - first split.
    Ts2_3: Duration of divergence with migration - second split.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    sts = moments.LinearSystem_1D.steady_state_1D(n1+n2+n3)
    phi = moments.Spectrum(sts)
    phi = moments.Manips.split_1D_to_2D(phi, n1, n2+n3)
    phi.integrate([nu1, nu23], Ts1_23)
    phi = moments.Manips.split_2D_to_3D_2(phi, n2, n3)
    phi.integrate([nu1, nu2, nu3], Ts2_3, m=numpy.array([[0, m1_2, m1_3],[m2_1, 0, m2_3],[m3_1, m3_2, 0]]))
    fsO = phi
    fsM = moments.Numerics.reverse_array(fsO)

    # Sum all spectra
    fs = O*fsO + (1-O)*fsM
    return fs


def P3_IM_SI(params, ns):
    nu1, nu23, nu2, nu3, m1_23, m23_1, Ts1_23, Ts2_3, O = params
    n1, n2, n3 = ns
    """
    Three populations: (pop1;(pop2;pop3)).
    Isolation with Migration during the first split, Strict Isolation during the second split.

    nu1: Size of population 1 after first split and until present.
    nu23: Size of ancestral population to (2,3) after first split.
    nu2: Size of population 2 after second split and until present.
    nu3: Size of population 3 after second split and until present.
    mi_j: Migration from pop j to pop i.
    Ts1_23: Duration of divergence with migration - first split.
    Ts2_3: Duration of divergence in isolation - second split.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    sts = moments.LinearSystem_1D.steady_state_1D(n1+n2+n3)
    phi = moments.Spectrum(sts)
    phi = moments.Manips.split_1D_to_2D(phi, n1, n2+n3)
    phi.integrate([nu1, nu23], Ts1_23, m=numpy.array([[0, m1_23],[m23_1, 0]]))
    phi = moments.Manips.split_2D_to_3D_2(phi, n2, n3)
    phi.integrate([nu1, nu2, nu3], Ts2_3)
    fsO = phi
    fsM = moments.Numerics.reverse_array(fsO)

    # Sum all spectra
    fs = O*fsO + (1-O)*fsM
    return fs

