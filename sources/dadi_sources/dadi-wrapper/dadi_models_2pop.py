#!/usr/local/bin/python
# -*- coding: utf-8 -*-

######################################################################################################################################################
#								IMPORT PACKAGES
######################################################################################################################################################
import sys
import dadi
import numpy
######################################################################################################################################################
#							STRICT ISOLATION MODELS - 2 POPULATIONS
######################################################################################################################################################
def SI(params, ns, pts):
    nu1, nu2, Ts, O = params
    n1,n2 = ns
    """
    Strict Isolation.

    nu1: Size of population 1 after split.
    nu2: Size of population 2 after split.
    Ts: Duration of divergence in isolation.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    # Equilibrium ancestral population
    phi = dadi.PhiManip.phi_1D(xx)
    # Split event
    phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
    # Integrate by setting divergence time to Ts, the population sizes to nu1 and nu2, and the migration rates to zero
    phi = dadi.Integration.two_pops(phi, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    # Calculate the well-oriented spectrum
    fsO = dadi.Spectrum.from_phi(phi, (n1,n2), (xx,xx))
    # Calculate the mis-oriented spectrum
    fsM = dadi.Numerics.reverse_array(fsO)

    # Sum all spectra
    fs = O*fsO + (1-O)*fsM
    return fs


def SIexp(params, ns, pts):
    nu1e, nu1, nu2e, nu2, Ts, Ti, O = params
    n1,n2 = ns
    """
    Strict Isolation with exponential size changes.

    nu1e: Size of population 1 after split.
    nu1: Size of population 1 after exponential size change.
    nu2e: Size of population 2 after split.
    nu2: Size of population 2 after exponential size change.
    Ts: Duration of divergence in isolation.
    Ti: Duration of divergence in isolation with pop1: exponential change / pop2: exponential change.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    phi = dadi.PhiManip.phi_1D(xx)
    phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
    phi = dadi.Integration.two_pops(phi, xx, T=Ts, nu1=nu1e, nu2=nu2e, m12=0, m21=0)
    nu1_func = lambda t: nu1e*(nu1/nu1e) ** (t/Ti)
    nu2_func = lambda t: nu2e*(nu2/nu2e) ** (t/Ti)
    phi = dadi.Integration.two_pops(phi, xx, T=Ti, nu1=nu1_func, nu2=nu2_func, m12=0, m21=0) 
    fsO = dadi.Spectrum.from_phi(phi, (n1,n2), (xx,xx))
    fsM = dadi.Numerics.reverse_array(fsO)

    # Sum all spectra
    fs = O*fsO + (1-O)*fsM
    return fs


def SI2N(params, ns, pts):
    nu1, nu2, Ts, nr, bf, O = params
    n1,n2 = ns
    """
    Strict Isolation with heterogeneous Ne (shared between populations).

    nu1: Size of population 1 after split.
    nu2: Size of population 2 after split.
    Ts: Duration of divergence in isolation.
    nr: Fraction of non-recombining regions, i.e. with reduced Ne.
    bf : Background factor that defines to which extent Ne is reduced.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    # Spectrum of non-recombining regions
    phinr = dadi.PhiManip.phi_1D(xx)
    phinr = dadi.PhiManip.phi_1D_to_2D(xx, phinr)
    phinr = dadi.Integration.two_pops(phinr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    fsnrO = dadi.Spectrum.from_phi(phinr, (n1,n2), (xx,xx))
    fsnrM = dadi.Numerics.reverse_array(fsnrO)
    
    # Spectrum of recombining regions
    phir = dadi.PhiManip.phi_1D(xx)
    phir = dadi.PhiManip.phi_1D_to_2D(xx, phir)
    phir = dadi.Integration.two_pops(phir, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsrO = dadi.Spectrum.from_phi(phir, (n1,n2), (xx,xx))
    fsrM = dadi.Numerics.reverse_array(fsrO)

    # Sum all spectra
    fs = O*( nr*fsnrO + (1-nr)*fsrO ) + (1-O)*( nr*fsnrM + (1-nr)*fsrM )
    return fs


######################################################################################################################################################
#							ISOLATION WITH MIGRATION MODELS - 2 POPULATIONS
######################################################################################################################################################
def IM(params, ns, pts):
    nu1, nu2, m12, m21, Ts, O = params
    n1,n2 = ns
    """
    Isolation with Migration.

    nu1: Size of population 1 after split.
    nu2: Size of population 2 after split.
    m12: Migration from pop 2 to pop 1.
    m21: Migration from pop 1 to pop 2.
    Ts: Duration of divergence with migration.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    phi = dadi.PhiManip.phi_1D(xx)
    phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
    phi = dadi.Integration.two_pops(phi, xx, T=Ts, nu1=nu1, nu2=nu2, m12=m12, m21=m21)
    fsO = dadi.Spectrum.from_phi(phi, (n1,n2), (xx,xx))
    fsM = dadi.Numerics.reverse_array(fsO)

    # Sum all spectra
    fs = O*fsO + (1-O)*fsM
    return fs


def IMexp(params, ns, pts):
    nu1e, nu1, nu2e, nu2, m12, m21, Ts, Ti, O = params
    n1,n2 = ns
    """
    Isolation with Migration with exponential size changes.

    nu1e: Size of population 1 after split.
    nu1: Size of population 1 after exponential size change.
    nu2e: Size of population 2 after split.
    nu2: Size of population 2 after exponential size change.
    m12: Migration from pop 2 to pop 1.
    m21: Migration from pop 1 to pop 2.
    Ts: Duration of divergence with migration.
    Ti: Duration of divergence with migration with pop1: exponential change / pop2: exponential change.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    phi = dadi.PhiManip.phi_1D(xx)
    phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
    phi = dadi.Integration.two_pops(phi, xx, T=Ts, nu1=nu1e, nu2=nu2e, m12=m12, m21=m21)
    nu1_func = lambda t: nu1e*(nu1/nu1e) ** (t/Ti)
    nu2_func = lambda t: nu2e*(nu2/nu2e) ** (t/Ti)
    phi = dadi.Integration.two_pops(phi, xx, T=Ti, nu1=nu1_func, nu2=nu2_func, m12=m12, m21=m21) 
    fsO = dadi.Spectrum.from_phi(phi, (n1,n2), (xx,xx))
    fsM = dadi.Numerics.reverse_array(fsO)

    # Sum all spectra
    fs = O*fsO + (1-O)*fsM
    return fs


def IM2N(params, ns, pts):
    nu1, nu2, m12, m21, Ts, nr, bf, O = params
    n1,n2 = ns
    """
    Isolation with Migration with heterogeneous Ne (shared between populations).

    nu1: Size of population 1 after split.
    nu2: Size of population 2 after split.
    m12: Migration from pop 2 to pop 1.
    m21: Migration from pop 1 to pop 2.
    Ts: Duration of divergence with migration.
    nr: Fraction of non-recombining regions, i.e. with reduced Ne.
    bf : Background factor that defines to which extent Ne is reduced.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    # Spectrum of non-recombining regions
    phinr = dadi.PhiManip.phi_1D(xx)
    phinr = dadi.PhiManip.phi_1D_to_2D(xx, phinr)
    phinr = dadi.Integration.two_pops(phinr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=m12, m21=m21)
    fsnrO = dadi.Spectrum.from_phi(phinr, (n1,n2), (xx,xx))
    fsnrM = dadi.Numerics.reverse_array(fsnrO)
    
    # Spectrum of recombining regions
    phir = dadi.PhiManip.phi_1D(xx)
    phir = dadi.PhiManip.phi_1D_to_2D(xx, phir)
    phir = dadi.Integration.two_pops(phir, xx, T=Ts, nu1=nu1, nu2=nu2, m12=m12, m21=m21)
    fsrO = dadi.Spectrum.from_phi(phir, (n1,n2), (xx,xx))
    fsrM = dadi.Numerics.reverse_array(fsrO)

    # Sum all spectra
    fs = O*( nr*fsnrO + (1-nr)*fsrO ) + (1-O)*( nr*fsnrM + (1-nr)*fsrM )
    return fs 


def IM2M(params, ns, pts):
    nu1, nu2, m12, m21, Ts, P, O = params
    n1,n2 = ns
    """
    Isolation with Migration with heterogeneous Me (shared between populations).

    nu1: Size of population 1 after split.
    nu2: Size of population 2 after split.
    m12: Migration from pop 2 to pop 1 in neutral regions. In barrier regions, m12=0.
    m21: Migration from pop 1 to pop 2 in neutral regions. In barrier regions, m21=0.
    Ts: Duration of divergence with migration.
    P: Fraction of the genome evolving neutrally.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    # Neutral spectrum
    phiN = dadi.PhiManip.phi_1D(xx)
    phiN = dadi.PhiManip.phi_1D_to_2D(xx, phiN)
    phiN = dadi.Integration.two_pops(phiN, xx, T=Ts, nu1=nu1, nu2=nu2, m12=m12, m21=m21)
    fsNO = dadi.Spectrum.from_phi(phiN, (n1,n2), (xx,xx))
    fsNM = dadi.Numerics.reverse_array(fsNO)

    # Barrier spectrum
    phiI = dadi.PhiManip.phi_1D(xx)
    phiI = dadi.PhiManip.phi_1D_to_2D(xx, phiI)
    phiI = dadi.Integration.two_pops(phiI, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsIO = dadi.Spectrum.from_phi(phiI, (n1,n2), (xx,xx))
    fsIM = dadi.Numerics.reverse_array(fsIO)

    # Sum all spectra
    fs = O*( P*fsNO + (1-P)*fsIO ) + (1-O)*( P*fsNM + (1-P)*fsIM )
    return fs
    

def IM2M2P(params, ns, pts):
    nu1, nu2, m12, m21, Ts, P1, P2, O = params
    n1,n2 = ns
    """
    Isolation with Migration with heterogeneous Me (population-specific).

    nu1: Size of population 1 after split.
    nu2: Size of population 2 after split.
    m12: Migration from pop 2 to pop 1 in neutral regions. In barrier regions, m12=0.
    m21: Migration from pop 1 to pop 2 in neutral regions. In barrier regions, m21=0.
    Ts: Duration of divergence with migration.
    P1: Fraction of the genome evolving neutrally in population 1.
    P2: Fraction of the genome evolving neutrally in population 2.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    # Neutral spectrum in population 1 and 2
    phiN1N2 = dadi.PhiManip.phi_1D(xx)
    phiN1N2 = dadi.PhiManip.phi_1D_to_2D(xx, phiN1N2)
    phiN1N2 = dadi.Integration.two_pops(phiN1N2, xx, T=Ts, nu1=nu1, nu2=nu2, m12=m12, m21=m21)
    fsN1N2O = dadi.Spectrum.from_phi(phiN1N2, (n1,n2), (xx,xx))
    fsN1N2M = dadi.Numerics.reverse_array(fsN1N2O)

    # Barrier spectrum in population 1 and 2
    phiI1I2 = dadi.PhiManip.phi_1D(xx)
    phiI1I2 = dadi.PhiManip.phi_1D_to_2D(xx, phiI1I2)
    phiI1I2 = dadi.Integration.two_pops(phiI1I2, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsI1I2O = dadi.Spectrum.from_phi(phiI1I2, (n1,n2), (xx,xx))
    fsI1I2M = dadi.Numerics.reverse_array(fsI1I2O)

    # Neutral spectrum in population 1, barrier spectrum in population 2
    phiN1I2 = dadi.PhiManip.phi_1D(xx)
    phiN1I2 = dadi.PhiManip.phi_1D_to_2D(xx, phiN1I2)
    phiN1I2 = dadi.Integration.two_pops(phiN1I2, xx, T=Ts, nu1=nu1, nu2=nu2, m12=m12, m21=0)
    fsN1I2O = dadi.Spectrum.from_phi(phiN1I2, (n1,n2), (xx,xx))
    fsN1I2M = dadi.Numerics.reverse_array(fsN1I2O)

    # Neutral spectrum in population 2, barrier spectrum in population 1
    phiI1N2 = dadi.PhiManip.phi_1D(xx)
    phiI1N2 = dadi.PhiManip.phi_1D_to_2D(xx, phiI1N2)
    phiI1N2 = dadi.Integration.two_pops(phiI1N2, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=m21)
    fsI1N2O = dadi.Spectrum.from_phi(phiI1N2, (n1,n2), (xx,xx))
    fsI1N2M = dadi.Numerics.reverse_array(fsI1N2O)

    # Sum all spectra
    fs = O*( P1*P2*fsN1N2O + (1-P1)*(1-P2)*fsI1I2O + P1*(1-P2)*fsN1I2O + (1-P1)*P2*fsI1N2O ) + (1-O)*( P1*P2*fsN1N2M + (1-P1)*(1-P2)*fsI1I2M + P1*(1-P2)*fsN1I2M + (1-P1)*P2*fsI1N2M )
    return fs


def IM2N2M(params, ns, pts):
    nu1, nu2, m12, m21, Ts, nr, bf, P, O = params
    n1,n2 = ns
    """
    Isolation with Migration with heterogeneous Ne (shared between populations) and heterogeneous Me (shared between populations).

    nu1: Size of population 1 after split.
    nu2: Size of population 2 after split.
    m12: Migration from pop 2 to pop 1 in neutral regions. In barrier regions, m12=0.
    m21: Migration from pop 1 to pop 2 in neutral regions. In barrier regions, m21=0.
    Ts: Duration of divergence with migration.
    nr: Fraction of non-recombining regions, i.e. with reduced Ne.
    bf : Background factor that defines to which extent Ne is reduced.
    P: Fraction of the genome evolving neutrally.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    # Neutral spectrum
    # Spectrum of non-recombining regions
    phiNnr = dadi.PhiManip.phi_1D(xx)
    phiNnr = dadi.PhiManip.phi_1D_to_2D(xx, phiNnr)
    phiNnr = dadi.Integration.two_pops(phiNnr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=m12, m21=m21)
    fsNOnr = dadi.Spectrum.from_phi(phiNnr, (n1,n2), (xx,xx))
    fsNMnr = dadi.Numerics.reverse_array(fsNOnr)
    # Spectrum of recombining regions
    phiNr = dadi.PhiManip.phi_1D(xx)
    phiNr = dadi.PhiManip.phi_1D_to_2D(xx, phiNr)
    phiNr = dadi.Integration.two_pops(phiNr, xx, T=Ts, nu1=nu1, nu2=nu2, m12=m12, m21=m21)
    fsNOr = dadi.Spectrum.from_phi(phiNr, (n1,n2), (xx,xx))
    fsNMr = dadi.Numerics.reverse_array(fsNOr)

    # Barrier spectrum
    # Spectrum of non-recombining regions
    phiInr = dadi.PhiManip.phi_1D(xx)
    phiInr = dadi.PhiManip.phi_1D_to_2D(xx, phiInr)
    phiInr = dadi.Integration.two_pops(phiInr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    fsIOnr = dadi.Spectrum.from_phi(phiInr, (n1,n2), (xx,xx))
    fsIMnr = dadi.Numerics.reverse_array(fsIOnr)
    # Spectrum of recombining regions
    phiIr = dadi.PhiManip.phi_1D(xx)
    phiIr = dadi.PhiManip.phi_1D_to_2D(xx, phiIr)
    phiIr = dadi.Integration.two_pops(phiIr, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsIOr = dadi.Spectrum.from_phi(phiIr, (n1,n2), (xx,xx))
    fsIMr = dadi.Numerics.reverse_array(fsIOr)

    # Sum all spectra
    fs = O*( P*nr*fsNOnr + P*(1-nr)*fsNOr + (1-P)*nr*fsIOnr + (1-P)*(1-nr)*fsIOr ) + (1-O)*( P*nr*fsNMnr + P*(1-nr)*fsNMr + (1-P)*nr*fsIMnr + (1-P)*(1-nr)*fsIMr )
    return fs


def IM2N2M2P(params, ns, pts):
    nu1, nu2, m12, m21, Ts, nr, bf, P1, P2, O = params
    n1,n2 = ns
    """
    Isolation with Migration with heterogeneous Ne (shared between populations) and heterogeneous Me (population-specific).

    nu1: Size of population 1 after split.
    nu2: Size of population 2 after split.
    m12: Migration from pop 2 to pop 1 in neutral regions. In barrier regions, m12=0.
    m21: Migration from pop 1 to pop 2 in neutral regions. In barrier regions, m21=0.
    Ts: Duration of divergence with migration.
    nr: Fraction of non-recombining regions, i.e. with reduced Ne.
    bf : Background factor that defines to which extent Ne is reduced.
    P1: Fraction of the genome evolving neutrally in population 1.
    P2: Fraction of the genome evolving neutrally in population 2.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    # Neutral spectrum in population 1 and 2
    # Spectrum of non-recombining regions
    phiN1N2nr = dadi.PhiManip.phi_1D(xx)
    phiN1N2nr = dadi.PhiManip.phi_1D_to_2D(xx, phiN1N2nr)
    phiN1N2nr = dadi.Integration.two_pops(phiN1N2nr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=m12, m21=m21)
    fsN1N2Onr = dadi.Spectrum.from_phi(phiN1N2nr, (n1,n2), (xx,xx))
    fsN1N2Mnr = dadi.Numerics.reverse_array(fsN1N2Onr)
    # Spectrum of recombining regions
    phiN1N2r = dadi.PhiManip.phi_1D(xx)
    phiN1N2r = dadi.PhiManip.phi_1D_to_2D(xx, phiN1N2r)
    phiN1N2r = dadi.Integration.two_pops(phiN1N2r, xx, T=Ts, nu1=nu1, nu2=nu2, m12=m12, m21=m21)
    fsN1N2Or = dadi.Spectrum.from_phi(phiN1N2r, (n1,n2), (xx,xx))
    fsN1N2Mr = dadi.Numerics.reverse_array(fsN1N2Or)

    # Barrier spectrum in population 1 and 2
    # Spectrum of non-recombining regions
    phiI1I2nr = dadi.PhiManip.phi_1D(xx)
    phiI1I2nr = dadi.PhiManip.phi_1D_to_2D(xx, phiI1I2nr)
    phiI1I2nr = dadi.Integration.two_pops(phiI1I2nr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    fsI1I2Onr = dadi.Spectrum.from_phi(phiI1I2nr, (n1,n2), (xx,xx))
    fsI1I2Mnr = dadi.Numerics.reverse_array(fsI1I2Onr)
    # Spectrum of recombining regions
    phiI1I2r = dadi.PhiManip.phi_1D(xx)
    phiI1I2r = dadi.PhiManip.phi_1D_to_2D(xx, phiI1I2r)
    phiI1I2r = dadi.Integration.two_pops(phiI1I2r, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsI1I2Or = dadi.Spectrum.from_phi(phiI1I2r, (n1,n2), (xx,xx))
    fsI1I2Mr = dadi.Numerics.reverse_array(fsI1I2Or)

    # Neutral spectrum in population 1, barrier spectrum in population 2
    # Spectrum of non-recombining regions
    phiN1I2nr = dadi.PhiManip.phi_1D(xx)
    phiN1I2nr = dadi.PhiManip.phi_1D_to_2D(xx, phiN1I2nr)
    phiN1I2nr = dadi.Integration.two_pops(phiN1I2nr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=m12, m21=0)
    fsN1I2Onr = dadi.Spectrum.from_phi(phiN1I2nr, (n1,n2), (xx,xx))
    fsN1I2Mnr = dadi.Numerics.reverse_array(fsN1I2Onr)
    # Spectrum of recombining regions
    phiN1I2r = dadi.PhiManip.phi_1D(xx)
    phiN1I2r = dadi.PhiManip.phi_1D_to_2D(xx, phiN1I2r)
    phiN1I2r = dadi.Integration.two_pops(phiN1I2r, xx, T=Ts, nu1=nu1, nu2=nu2, m12=m12, m21=0)
    fsN1I2Or = dadi.Spectrum.from_phi(phiN1I2r, (n1,n2), (xx,xx))
    fsN1I2Mr = dadi.Numerics.reverse_array(fsN1I2Or)

    # Neutral spectrum in population 2, barrier spectrum in population 1
    # Spectrum of non-recombining regions
    phiI1N2nr = dadi.PhiManip.phi_1D(xx)
    phiI1N2nr = dadi.PhiManip.phi_1D_to_2D(xx, phiI1N2nr)
    phiI1N2nr = dadi.Integration.two_pops(phiI1N2nr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=m21)
    fsI1N2Onr = dadi.Spectrum.from_phi(phiI1N2nr, (n1,n2), (xx,xx))
    fsI1N2Mnr = dadi.Numerics.reverse_array(fsI1N2Onr)
    # Spectrum of recombining regions
    phiI1N2r = dadi.PhiManip.phi_1D(xx)
    phiI1N2r = dadi.PhiManip.phi_1D_to_2D(xx, phiI1N2r)
    phiI1N2r = dadi.Integration.two_pops(phiI1N2r, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=m21)
    fsI1N2Or = dadi.Spectrum.from_phi(phiI1N2r, (n1,n2), (xx,xx))
    fsI1N2Mr = dadi.Numerics.reverse_array(fsI1N2Or)

    # Sum all spectra
    fs = O*( P1*P2*nr*fsN1N2Onr + P1*P2*(1-nr)*fsN1N2Or + (1-P1)*(1-P2)*nr*fsI1I2Onr + (1-P1)*(1-P2)*(1-nr)*fsI1I2Or + P1*(1-P2)*nr*fsN1I2Onr + P1*(1-P2)*(1-nr)*fsN1I2Or + (1-P1)*P2*nr*fsI1N2Onr + (1-P1)*P2*(1-nr)*fsI1N2Or ) + (1-O)*( P1*P2*nr*fsN1N2Mnr + P1*P2*(1-nr)*fsN1N2Mr + (1-P1)*(1-P2)*nr*fsI1I2Mnr + (1-P1)*(1-P2)*(1-nr)*fsI1I2Mr + P1*(1-P2)*nr*fsN1I2Mnr + P1*(1-P2)*(1-nr)*fsN1I2Mr + (1-P1)*P2*nr*fsI1N2Mnr + (1-P1)*P2*(1-nr)*fsI1N2Mr )
    return fs


######################################################################################################################################################
#							SECONDARY CONTACT MODELS - 2 POPULATIONS
######################################################################################################################################################
def SC(params, ns, pts):
    nu1, nu2, m12, m21, Ts, Ti, O = params
    n1,n2 = ns
    """
    Secondary Contact.

    nu1: Size of population 1 after split.
    nu2: Size of population 2 after split.
    m12: Migration from pop 2 to pop 1.
    m21: Migration from pop 1 to pop 2.
    Ts: Duration of divergence in isolation.
    Ti: Duration of divergence with migration.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    phi = dadi.PhiManip.phi_1D(xx)
    phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
    phi = dadi.Integration.two_pops(phi, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    phi = dadi.Integration.two_pops(phi, xx, T=Ti, nu1=nu1, nu2=nu2, m12=m12, m21=m21)
    fsO = dadi.Spectrum.from_phi(phi, (n1,n2), (xx,xx))
    fsM = dadi.Numerics.reverse_array(fsO)

    # Sum all spectra
    fs = O*fsO + (1-O)*fsM
    return fs


def SCexp(params, ns, pts):
    nu1e, nu1, nu2e, nu2, m12, m21, Ts, Ti, O = params
    n1,n2 = ns
    """
    Secondary Contact with exponential size changes.

    nu1e: Size of population 1 after split.
    nu1: Size of population 1 after exponential size change.
    nu2e: Size of population 2 after split.
    nu2: Size of population 2 after exponential size change.
    m12: Migration from pop 2 to pop 1.
    m21: Migration from pop 1 to pop 2.
    Ts: Duration of divergence in isolation.
    Ti: Duration of divergence with migration with pop1: exponential change / pop2: exponential change.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    phi = dadi.PhiManip.phi_1D(xx)
    phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
    phi = dadi.Integration.two_pops(phi, xx, T=Ts, nu1=nu1e, nu2=nu2e, m12=0, m21=0)
    nu1_func = lambda t: nu1e*(nu1/nu1e) ** (t/Ti)
    nu2_func = lambda t: nu2e*(nu2/nu2e) ** (t/Ti)
    phi = dadi.Integration.two_pops(phi, xx, T=Ti, nu1=nu1_func, nu2=nu2_func, m12=m12, m21=m21) 
    fsO = dadi.Spectrum.from_phi(phi, (n1,n2), (xx,xx))
    fsM = dadi.Numerics.reverse_array(fsO)

    # Sum all spectra
    fs = O*fsO + (1-O)*fsM
    return fs


def SC2N(params, ns, pts):
    nu1, nu2, m12, m21, Ts, Ti, nr, bf, O = params
    n1,n2 = ns
    """
    Secondary Contact with heterogeneous Ne (shared between populations).

    nu1: Size of population 1 after split.
    nu2: Size of population 2 after split.
    m12: Migration from pop 2 to pop 1.
    m21: Migration from pop 1 to pop 2.
    Ts: Duration of divergence in isolation.
    Ti: Duration of divergence with migration.
    nr: Fraction of non-recombining regions, i.e. with reduced Ne.
    bf : Background factor that defines to which extent Ne is reduced.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """   
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    # Spectrum of non-recombining regions
    phinr = dadi.PhiManip.phi_1D(xx)
    phinr = dadi.PhiManip.phi_1D_to_2D(xx, phinr)
    phinr = dadi.Integration.two_pops(phinr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    phinr = dadi.Integration.two_pops(phinr, xx, T=Ti, nu1=nu1*bf, nu2=nu2*bf, m12=m12, m21=m21)
    fsnrO = dadi.Spectrum.from_phi(phinr, (n1,n2), (xx,xx))
    fsnrM = dadi.Numerics.reverse_array(fsnrO)
    
    # Spectrum of recombining regions
    phir = dadi.PhiManip.phi_1D(xx)
    phir = dadi.PhiManip.phi_1D_to_2D(xx, phir)
    phir = dadi.Integration.two_pops(phir, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    phir = dadi.Integration.two_pops(phir, xx, T=Ti, nu1=nu1, nu2=nu2, m12=m12, m21=m21)
    fsrO = dadi.Spectrum.from_phi(phir, (n1,n2), (xx,xx))
    fsrM = dadi.Numerics.reverse_array(fsrO)

    # Sum all spectra
    fs = O*( nr*fsnrO + (1-nr)*fsrO ) + (1-O)*( nr*fsnrM + (1-nr)*fsrM )
    return fs


def SC2M(params, ns, pts):
    nu1, nu2, m12, m21, Ts, Ti, P, O = params
    n1,n2 = ns
    """
    Secondary Contact with heterogeneous Me (shared between populations).

    nu1: Size of population 1 after split.
    nu2: Size of population 2 after split.
    m12: Migration from pop 2 to pop 1 in neutral regions. In barrier regions, m12=0.
    m21: Migration from pop 1 to pop 2 in neutral regions. In barrier regions, m21=0.
    Ts: Duration of divergence in isolation.
    Ti: Duration of divergence with migration.
    P: Fraction of the genome evolving neutrally.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    # Neutral spectrum
    phiN = dadi.PhiManip.phi_1D(xx)
    phiN = dadi.PhiManip.phi_1D_to_2D(xx, phiN)
    phiN = dadi.Integration.two_pops(phiN, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    phiN = dadi.Integration.two_pops(phiN, xx, T=Ti, nu1=nu1, nu2=nu2, m12=m12, m21=m21)
    fsNO = dadi.Spectrum.from_phi(phiN, (n1,n2), (xx,xx))
    fsNM = dadi.Numerics.reverse_array(fsNO)

    # Barrier spectrum
    phiI = dadi.PhiManip.phi_1D(xx)
    phiI = dadi.PhiManip.phi_1D_to_2D(xx, phiI)
    phiI = dadi.Integration.two_pops(phiI, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    phiI = dadi.Integration.two_pops(phiI, xx, T=Ti, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsIO = dadi.Spectrum.from_phi(phiI, (n1,n2), (xx,xx))
    fsIM = dadi.Numerics.reverse_array(fsIO)

    # Sum all spectra
    fs = O*( P*fsNO + (1-P)*fsIO ) + (1-O)*( P*fsNM + (1-P)*fsIM )
    return fs


def SC2M2P(params, ns, pts):
    nu1, nu2, m12, m21, Ts, Ti, P1, P2, O = params
    n1,n2 = ns
    """
    Secondary Contact with heterogeneous Me (population-specific).

    nu1: Size of population 1 after split.
    nu2: Size of population 2 after split.
    m12: Migration from pop 2 to pop 1 in neutral regions. In barrier regions, m12=0.
    m21: Migration from pop 1 to pop 2 in neutral regions. In barrier regions, m21=0.
    Ts: Duration of divergence in isolation.
    Ti: Duration of divergence with migration.
    P1: Fraction of the genome evolving neutrally in population 1.
    P2: Fraction of the genome evolving neutrally in population 2.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    # Neutral spectrum in population 1 and 2
    phiN1N2 = dadi.PhiManip.phi_1D(xx)
    phiN1N2 = dadi.PhiManip.phi_1D_to_2D(xx, phiN1N2)
    phiN1N2 = dadi.Integration.two_pops(phiN1N2, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    phiN1N2 = dadi.Integration.two_pops(phiN1N2, xx, T=Ti, nu1=nu1, nu2=nu2, m12=m12, m21=m21)
    fsN1N2O = dadi.Spectrum.from_phi(phiN1N2, (n1,n2), (xx,xx))
    fsN1N2M = dadi.Numerics.reverse_array(fsN1N2O)

    # Barrier spectrum in population 1 and 2
    phiI1I2 = dadi.PhiManip.phi_1D(xx)
    phiI1I2 = dadi.PhiManip.phi_1D_to_2D(xx, phiI1I2)
    phiI1I2 = dadi.Integration.two_pops(phiI1I2, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    phiI1I2 = dadi.Integration.two_pops(phiI1I2, xx, T=Ti, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsI1I2O = dadi.Spectrum.from_phi(phiI1I2, (n1,n2), (xx,xx))
    fsI1I2M = dadi.Numerics.reverse_array(fsI1I2O)

    # Neutral spectrum in population 1, barrier spectrum in population 2
    phiN1I2 = dadi.PhiManip.phi_1D(xx)
    phiN1I2 = dadi.PhiManip.phi_1D_to_2D(xx, phiN1I2)
    phiN1I2 = dadi.Integration.two_pops(phiN1I2, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    phiN1I2 = dadi.Integration.two_pops(phiN1I2, xx, T=Ti, nu1=nu1, nu2=nu2, m12=m12, m21=0)
    fsN1I2O = dadi.Spectrum.from_phi(phiN1I2, (n1,n2), (xx,xx))
    fsN1I2M = dadi.Numerics.reverse_array(fsN1I2O)

    # Neutral spectrum in population 2, barrier spectrum in population 1
    phiI1N2 = dadi.PhiManip.phi_1D(xx)
    phiI1N2 = dadi.PhiManip.phi_1D_to_2D(xx, phiI1N2)
    phiI1N2 = dadi.Integration.two_pops(phiI1N2, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    phiI1N2 = dadi.Integration.two_pops(phiI1N2, xx, T=Ti, nu1=nu1, nu2=nu2, m12=0, m21=m21)
    fsI1N2O = dadi.Spectrum.from_phi(phiI1N2, (n1,n2), (xx,xx))
    fsI1N2M = dadi.Numerics.reverse_array(fsI1N2O)

    # Sum all spectra
    fs = O*( P1*P2*fsN1N2O + (1-P1)*(1-P2)*fsI1I2O + P1*(1-P2)*fsN1I2O + (1-P1)*P2*fsI1N2O ) + (1-O)*( P1*P2*fsN1N2M + (1-P1)*(1-P2)*fsI1I2M + P1*(1-P2)*fsN1I2M + (1-P1)*P2*fsI1N2M )
    return fs


def SC2N2M(params, ns, pts):
    nu1, nu2, m12, m21, Ts, Ti, nr, bf, P, O = params
    n1,n2 = ns
    """
    Secondary Contact with heterogeneous Ne (shared between populations) and heterogeneous Me (shared between populations).

    nu1: Size of population 1 after split.
    nu2: Size of population 2 after split.
    m12: Migration from pop 2 to pop 1 in neutral regions. In barrier regions, m12=0.
    m21: Migration from pop 1 to pop 2 in neutral regions. In barrier regions, m21=0.
    Ts: Duration of divergence in isolation.
    Ti: Duration of divergence with migration.
    nr: Fraction of non-recombining regions, i.e. with reduced Ne.
    bf : Background factor that defines to which extent Ne is reduced.
    P: Fraction of the genome evolving neutrally.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    # Neutral spectrum
    # Spectrum of non-recombining regions
    phiNnr = dadi.PhiManip.phi_1D(xx)
    phiNnr = dadi.PhiManip.phi_1D_to_2D(xx, phiNnr)
    phiNnr = dadi.Integration.two_pops(phiNnr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    phiNnr = dadi.Integration.two_pops(phiNnr, xx, T=Ti, nu1=nu1*bf, nu2=nu2*bf, m12=m12, m21=m21)
    fsNOnr = dadi.Spectrum.from_phi(phiNnr, (n1,n2), (xx,xx))
    fsNMnr = dadi.Numerics.reverse_array(fsNOnr)
    # Spectrum of recombining regions
    phiNr = dadi.PhiManip.phi_1D(xx)
    phiNr = dadi.PhiManip.phi_1D_to_2D(xx, phiNr)
    phiNr = dadi.Integration.two_pops(phiNr, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    phiNr = dadi.Integration.two_pops(phiNr, xx, T=Ti, nu1=nu1, nu2=nu2, m12=m12, m21=m21)
    fsNOr = dadi.Spectrum.from_phi(phiNr, (n1,n2), (xx,xx))
    fsNMr = dadi.Numerics.reverse_array(fsNOr)

    # Barrier spectrum
    # Spectrum of non-recombining regions
    phiInr = dadi.PhiManip.phi_1D(xx)
    phiInr = dadi.PhiManip.phi_1D_to_2D(xx, phiInr)
    phiInr = dadi.Integration.two_pops(phiInr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    phiInr = dadi.Integration.two_pops(phiInr, xx, T=Ti, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    fsIOnr = dadi.Spectrum.from_phi(phiInr, (n1,n2), (xx,xx))
    fsIMnr = dadi.Numerics.reverse_array(fsIOnr)
    # Spectrum of recombining regions
    phiIr = dadi.PhiManip.phi_1D(xx)
    phiIr = dadi.PhiManip.phi_1D_to_2D(xx, phiIr)
    phiIr = dadi.Integration.two_pops(phiIr, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    phiIr = dadi.Integration.two_pops(phiIr, xx, T=Ti, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsIOr = dadi.Spectrum.from_phi(phiIr, (n1,n2), (xx,xx))
    fsIMr = dadi.Numerics.reverse_array(fsIOr)

    # Sum all spectra
    fs = O*( P*nr*fsNOnr + P*(1-nr)*fsNOr + (1-P)*nr*fsIOnr + (1-P)*(1-nr)*fsIOr ) + (1-O)*( P*nr*fsNMnr + P*(1-nr)*fsNMr + (1-P)*nr*fsIMnr + (1-P)*(1-nr)*fsIMr )
    return fs


def SC2N2M2P(params, ns, pts):
    nu1, nu2, m12, m21, Ts, Ti, nr, bf, P1, P2, O = params
    n1,n2 = ns
    """
    Secondary Contact with heterogeneous Ne (shared between populations) and heterogeneous Me (population-specific).

    nu1: Size of population 1 after split.
    nu2: Size of population 2 after split.
    m12: Migration from pop 2 to pop 1 in neutral regions. In barrier regions, m12=0.
    m21: Migration from pop 1 to pop 2 in neutral regions. In barrier regions, m21=0.
    Ts: Duration of divergence in isolation.
    Ti: Duration of divergence with migration.
    nr: Fraction of non-recombining regions, i.e. with reduced Ne.
    bf : Background factor that defines to which extent Ne is reduced.
    P1: Fraction of the genome evolving neutrally in population 1.
    P2: Fraction of the genome evolving neutrally in population 2.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    # Neutral spectrum in population 1 and 2
    # Spectrum of non-recombining regions
    phiN1N2nr = dadi.PhiManip.phi_1D(xx)
    phiN1N2nr = dadi.PhiManip.phi_1D_to_2D(xx, phiN1N2nr)
    phiN1N2nr = dadi.Integration.two_pops(phiN1N2nr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    phiN1N2nr = dadi.Integration.two_pops(phiN1N2nr, xx, T=Ti, nu1=nu1*bf, nu2=nu2*bf, m12=m12, m21=m21)
    fsN1N2Onr = dadi.Spectrum.from_phi(phiN1N2nr, (n1,n2), (xx,xx))
    fsN1N2Mnr = dadi.Numerics.reverse_array(fsN1N2Onr)
    # Spectrum of recombining regions
    phiN1N2r = dadi.PhiManip.phi_1D(xx)
    phiN1N2r = dadi.PhiManip.phi_1D_to_2D(xx, phiN1N2r)
    phiN1N2r = dadi.Integration.two_pops(phiN1N2r, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    phiN1N2r = dadi.Integration.two_pops(phiN1N2r, xx, T=Ti, nu1=nu1, nu2=nu2, m12=m12, m21=m21)
    fsN1N2Or = dadi.Spectrum.from_phi(phiN1N2r, (n1,n2), (xx,xx))
    fsN1N2Mr = dadi.Numerics.reverse_array(fsN1N2Or)

    # Barrier spectrum in population 1 and 2
    # Spectrum of non-recombining regions
    phiI1I2nr = dadi.PhiManip.phi_1D(xx)
    phiI1I2nr = dadi.PhiManip.phi_1D_to_2D(xx, phiI1I2nr)
    phiI1I2nr = dadi.Integration.two_pops(phiI1I2nr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    phiI1I2nr = dadi.Integration.two_pops(phiI1I2nr, xx, T=Ti, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    fsI1I2Onr = dadi.Spectrum.from_phi(phiI1I2nr, (n1,n2), (xx,xx))
    fsI1I2Mnr = dadi.Numerics.reverse_array(fsI1I2Onr)
    # Spectrum of recombining regions
    phiI1I2r = dadi.PhiManip.phi_1D(xx)
    phiI1I2r = dadi.PhiManip.phi_1D_to_2D(xx, phiI1I2r)
    phiI1I2r = dadi.Integration.two_pops(phiI1I2r, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    phiI1I2r = dadi.Integration.two_pops(phiI1I2r, xx, T=Ti, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsI1I2Or = dadi.Spectrum.from_phi(phiI1I2r, (n1,n2), (xx,xx))
    fsI1I2Mr = dadi.Numerics.reverse_array(fsI1I2Or)

    # Neutral spectrum in population 1, barrier spectrum in population 2
    # Spectrum of non-recombining regions
    phiN1I2nr = dadi.PhiManip.phi_1D(xx)
    phiN1I2nr = dadi.PhiManip.phi_1D_to_2D(xx, phiN1I2nr)
    phiN1I2nr = dadi.Integration.two_pops(phiN1I2nr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    phiN1I2nr = dadi.Integration.two_pops(phiN1I2nr, xx, T=Ti, nu1=nu1*bf, nu2=nu2*bf, m12=m12, m21=0)
    fsN1I2Onr = dadi.Spectrum.from_phi(phiN1I2nr, (n1,n2), (xx,xx))
    fsN1I2Mnr = dadi.Numerics.reverse_array(fsN1I2Onr)
    # Spectrum of recombining regions
    phiN1I2r = dadi.PhiManip.phi_1D(xx)
    phiN1I2r = dadi.PhiManip.phi_1D_to_2D(xx, phiN1I2r)
    phiN1I2r = dadi.Integration.two_pops(phiN1I2r, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    phiN1I2r = dadi.Integration.two_pops(phiN1I2r, xx, T=Ti, nu1=nu1, nu2=nu2, m12=m12, m21=0)
    fsN1I2Or = dadi.Spectrum.from_phi(phiN1I2r, (n1,n2), (xx,xx))
    fsN1I2Mr = dadi.Numerics.reverse_array(fsN1I2Or)

    # Neutral spectrum in population 2, barrier spectrum in population 1
    # Spectrum of non-recombining regions
    phiI1N2nr = dadi.PhiManip.phi_1D(xx)
    phiI1N2nr = dadi.PhiManip.phi_1D_to_2D(xx, phiI1N2nr)
    phiI1N2nr = dadi.Integration.two_pops(phiI1N2nr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    phiI1N2nr = dadi.Integration.two_pops(phiI1N2nr, xx, T=Ti, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=m21)
    fsI1N2Onr = dadi.Spectrum.from_phi(phiI1N2nr, (n1,n2), (xx,xx))
    fsI1N2Mnr = dadi.Numerics.reverse_array(fsI1N2Onr)
    # Spectrum of recombining regions
    phiI1N2r = dadi.PhiManip.phi_1D(xx)
    phiI1N2r = dadi.PhiManip.phi_1D_to_2D(xx, phiI1N2r)
    phiI1N2r = dadi.Integration.two_pops(phiI1N2r, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    phiI1N2r = dadi.Integration.two_pops(phiI1N2r, xx, T=Ti, nu1=nu1, nu2=nu2, m12=0, m21=m21)
    fsI1N2Or = dadi.Spectrum.from_phi(phiI1N2r, (n1,n2), (xx,xx))
    fsI1N2Mr = dadi.Numerics.reverse_array(fsI1N2Or)

    # Sum all spectra
    fs = O*( P1*P2*nr*fsN1N2Onr + P1*P2*(1-nr)*fsN1N2Or + (1-P1)*(1-P2)*nr*fsI1I2Onr + (1-P1)*(1-P2)*(1-nr)*fsI1I2Or + P1*(1-P2)*nr*fsN1I2Onr + P1*(1-P2)*(1-nr)*fsN1I2Or + (1-P1)*P2*nr*fsI1N2Onr + (1-P1)*P2*(1-nr)*fsI1N2Or ) + (1-O)*( P1*P2*nr*fsN1N2Mnr + P1*P2*(1-nr)*fsN1N2Mr + (1-P1)*(1-P2)*nr*fsI1I2Mnr + (1-P1)*(1-P2)*(1-nr)*fsI1I2Mr + P1*(1-P2)*nr*fsN1I2Mnr + P1*(1-P2)*(1-nr)*fsN1I2Mr + (1-P1)*P2*nr*fsI1N2Mnr + (1-P1)*P2*(1-nr)*fsI1N2Mr )
    return fs


######################################################################################################################################################
#							ANCIENT MIGRATION MODELS - 2 POPULATIONS
######################################################################################################################################################
def AM(params, ns, pts):
    nu1, nu2, m12, m21, Ts, Ti, O = params
    n1,n2 = ns
    """
    Ancient Migration.

    nu1: Size of population 1 after split.
    nu2: Size of population 2 after split.
    m12: Migration from pop 2 to pop 1.
    m21: Migration from pop 1 to pop 2.
    Ts: Duration of divergence with migration.
    Ti: Duration of divergence in isolation.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    phi = dadi.PhiManip.phi_1D(xx)
    phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
    phi = dadi.Integration.two_pops(phi, xx, T=Ts, nu1=nu1, nu2=nu2, m12=m12, m21=m21)
    phi = dadi.Integration.two_pops(phi, xx, T=Ti, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsO = dadi.Spectrum.from_phi(phi, (n1,n2), (xx,xx))
    fsM = dadi.Numerics.reverse_array(fsO)

    # Sum all spectra
    fs = O*fsO + (1-O)*fsM
    return fs


def AMexp(params, ns, pts):
    nu1e, nu1, nu2e, nu2, m12, m21, Ts, Ti, O = params
    n1,n2 = ns
    """
    Ancient Migration with exponential size changes.

    nu1e: Size of population 1 after split.
    nu1: Size of population 1 after exponential size change.
    nu2e: Size of population 2 after split.
    nu2: Size of population 2 after exponential size change.
    m12: Migration from pop 2 to pop 1.
    m21: Migration from pop 1 to pop 2.
    Ts: Duration of divergence with migration.
    Ti: Duration of divergence in isolation with pop1: exponential change / pop2: exponential change.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    phi = dadi.PhiManip.phi_1D(xx)
    phi = dadi.PhiManip.phi_1D_to_2D(xx, phi)
    phi = dadi.Integration.two_pops(phi, xx, T=Ts, nu1=nu1e, nu2=nu2e, m12=m12, m21=m21)
    nu1_func = lambda t: nu1e*(nu1/nu1e) ** (t/Ti)
    nu2_func = lambda t: nu2e*(nu2/nu2e) ** (t/Ti)
    phi = dadi.Integration.two_pops(phi, xx, T=Ti, nu1=nu1_func, nu2=nu2_func, m12=0, m21=0) 
    fsO = dadi.Spectrum.from_phi(phi, (n1,n2), (xx,xx))
    fsM = dadi.Numerics.reverse_array(fsO)

    # Sum all spectra
    fs = O*fsO + (1-O)*fsM
    return fs


def AM2N(params, ns, pts):
    nu1, nu2, m12, m21, Ts, Ti, nr, bf, O = params
    n1,n2 = ns
    """
    Ancient Migration with heterogeneous Ne (shared between populations).

    nu1: Size of population 1 after split.
    nu2: Size of population 2 after split.
    m12: Migration from pop 2 to pop 1.
    m21: Migration from pop 1 to pop 2.
    Ts: Duration of divergence with migration.
    Ti: Duration of divergence in isolation.
    nr: Fraction of non-recombining regions, i.e. with reduced Ne.
    bf : Background factor that defines to which extent Ne is reduced.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    # Spectrum of non-recombining regions
    phinr = dadi.PhiManip.phi_1D(xx)
    phinr = dadi.PhiManip.phi_1D_to_2D(xx, phinr)
    phinr = dadi.Integration.two_pops(phinr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=m12, m21=m21)
    phinr = dadi.Integration.two_pops(phinr, xx, T=Ti, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    fsnrO = dadi.Spectrum.from_phi(phinr, (n1,n2), (xx,xx))
    fsnrM = dadi.Numerics.reverse_array(fsnrO)
    
    # Spectrum of recombining regions
    phir = dadi.PhiManip.phi_1D(xx)
    phir = dadi.PhiManip.phi_1D_to_2D(xx, phir)
    phir = dadi.Integration.two_pops(phir, xx, T=Ts, nu1=nu1, nu2=nu2, m12=m12, m21=m21)
    phir = dadi.Integration.two_pops(phir, xx, T=Ti, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsrO = dadi.Spectrum.from_phi(phir, (n1,n2), (xx,xx))
    fsrM = dadi.Numerics.reverse_array(fsrO)

    # Sum all spectra
    fs = O*( nr*fsnrO + (1-nr)*fsrO ) + (1-O)*( nr*fsnrM + (1-nr)*fsrM )
    return fs


def AM2M(params, ns, pts):
    nu1, nu2, m12, m21, Ts, Ti, P, O = params
    n1,n2 = ns
    """
    Ancient Migration with heterogeneous Me (shared between populations).

    nu1: Size of population 1 after split.
    nu2: Size of population 2 after split.
    m12: Migration from pop 2 to pop 1 in neutral regions. In barrier regions, m12=0.
    m21: Migration from pop 1 to pop 2 in neutral regions. In barrier regions, m21=0.
    Ts: Duration of divergence with migration.
    Ti: Duration of divergence in isolation.
    P: Fraction of the genome evolving neutrally.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    # Neutral spectrum
    phiN = dadi.PhiManip.phi_1D(xx)
    phiN = dadi.PhiManip.phi_1D_to_2D(xx, phiN)
    phiN = dadi.Integration.two_pops(phiN, xx, T=Ts, nu1=nu1, nu2=nu2, m12=m12, m21=m21)
    phiN = dadi.Integration.two_pops(phiN, xx, T=Ti, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsNO = dadi.Spectrum.from_phi(phiN, (n1,n2), (xx,xx))
    fsNM = dadi.Numerics.reverse_array(fsNO)

    # Barrier spectrum
    phiI = dadi.PhiManip.phi_1D(xx)
    phiI = dadi.PhiManip.phi_1D_to_2D(xx, phiI)
    phiI = dadi.Integration.two_pops(phiI, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    phiI = dadi.Integration.two_pops(phiI, xx, T=Ti, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsIO = dadi.Spectrum.from_phi(phiI, (n1,n2), (xx,xx))
    fsIM = dadi.Numerics.reverse_array(fsIO)

    # Sum all spectra
    fs = O*( P*fsNO + (1-P)*fsIO ) + (1-O)*( P*fsNM + (1-P)*fsIM )
    return fs


def AM2M2P(params, ns, pts):
    nu1, nu2, m12, m21, Ts, Ti, P1, P2, O = params
    n1,n2 = ns
    """
    Ancient Migration with heterogeneous Me (population-specific).

    nu1: Size of population 1 after split.
    nu2: Size of population 2 after split.
    m12: Migration from pop 2 to pop 1 in neutral regions. In barrier regions, m12=0.
    m21: Migration from pop 1 to pop 2 in neutral regions. In barrier regions, m21=0.
    Ts: Duration of divergence with migration.
    Ti: Duration of divergence in isolation.
    P1: Fraction of the genome evolving neutrally in population 1.
    P2: Fraction of the genome evolving neutrally in population 2.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    # Neutral spectrum in population 1 and 2
    phiN1N2 = dadi.PhiManip.phi_1D(xx)
    phiN1N2 = dadi.PhiManip.phi_1D_to_2D(xx, phiN1N2)
    phiN1N2 = dadi.Integration.two_pops(phiN1N2, xx, T=Ts, nu1=nu1, nu2=nu2, m12=m12, m21=m21)
    phiN1N2 = dadi.Integration.two_pops(phiN1N2, xx, T=Ti, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsN1N2O = dadi.Spectrum.from_phi(phiN1N2, (n1,n2), (xx,xx))
    fsN1N2M = dadi.Numerics.reverse_array(fsN1N2O)

    # Barrier spectrum in population 1 and 2
    phiI1I2 = dadi.PhiManip.phi_1D(xx)
    phiI1I2 = dadi.PhiManip.phi_1D_to_2D(xx, phiI1I2)
    phiI1I2 = dadi.Integration.two_pops(phiI1I2, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    phiI1I2 = dadi.Integration.two_pops(phiI1I2, xx, T=Ti, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsI1I2O = dadi.Spectrum.from_phi(phiI1I2, (n1,n2), (xx,xx))
    fsI1I2M = dadi.Numerics.reverse_array(fsI1I2O)

    # Neutral spectrum in population 1, barrier spectrum in population 2
    phiN1I2 = dadi.PhiManip.phi_1D(xx)
    phiN1I2 = dadi.PhiManip.phi_1D_to_2D(xx, phiN1I2)
    phiN1I2 = dadi.Integration.two_pops(phiN1I2, xx, T=Ts, nu1=nu1, nu2=nu2, m12=m12, m21=0)
    phiN1I2 = dadi.Integration.two_pops(phiN1I2, xx, T=Ti, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsN1I2O = dadi.Spectrum.from_phi(phiN1I2, (n1,n2), (xx,xx))
    fsN1I2M = dadi.Numerics.reverse_array(fsN1I2O)

    # Neutral spectrum in population 2, barrier spectrum in population 1
    phiI1N2 = dadi.PhiManip.phi_1D(xx)
    phiI1N2 = dadi.PhiManip.phi_1D_to_2D(xx, phiI1N2)
    phiI1N2 = dadi.Integration.two_pops(phiI1N2, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=m21)
    phiI1N2 = dadi.Integration.two_pops(phiI1N2, xx, T=Ti, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsI1N2O = dadi.Spectrum.from_phi(phiI1N2, (n1,n2), (xx,xx))
    fsI1N2M = dadi.Numerics.reverse_array(fsI1N2O)

    # Sum all spectra
    fs = O*( P1*P2*fsN1N2O + (1-P1)*(1-P2)*fsI1I2O + P1*(1-P2)*fsN1I2O + (1-P1)*P2*fsI1N2O ) + (1-O)*( P1*P2*fsN1N2M + (1-P1)*(1-P2)*fsI1I2M + P1*(1-P2)*fsN1I2M + (1-P1)*P2*fsI1N2M )
    return fs


def AM2N2M(params, ns, pts):
    nu1, nu2, m12, m21, Ts, Ti, nr, bf, P, O = params
    n1,n2 = ns
    """
    Ancient Migration with heterogeneous Ne (shared between populations) and heterogeneous Me (shared between populations).

    nu1: Size of population 1 after split.
    nu2: Size of population 2 after split.
    m12: Migration from pop 2 to pop 1 in neutral regions. In barrier regions, m12=0.
    m21: Migration from pop 1 to pop 2 in neutral regions. In barrier regions, m21=0.
    Ts: Duration of divergence with migration.
    Ti: Duration of divergence in isolation.
    nr: Fraction of non-recombining regions, i.e. with reduced Ne.
    bf : Background factor that defines to which extent Ne is reduced.
    P: Fraction of the genome evolving neutrally.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    # Neutral spectrum
    # Spectrum of non-recombining regions
    phiNnr = dadi.PhiManip.phi_1D(xx)
    phiNnr = dadi.PhiManip.phi_1D_to_2D(xx, phiNnr)
    phiNnr = dadi.Integration.two_pops(phiNnr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=m12, m21=m21)
    phiNnr = dadi.Integration.two_pops(phiNnr, xx, T=Ti, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    fsNOnr = dadi.Spectrum.from_phi(phiNnr, (n1,n2), (xx,xx))
    fsNMnr = dadi.Numerics.reverse_array(fsNOnr)
    # Spectrum of recombining regions
    phiNr = dadi.PhiManip.phi_1D(xx)
    phiNr = dadi.PhiManip.phi_1D_to_2D(xx, phiNr)
    phiNr = dadi.Integration.two_pops(phiNr, xx, T=Ts, nu1=nu1, nu2=nu2, m12=m12, m21=m21)
    phiNr = dadi.Integration.two_pops(phiNr, xx, T=Ti, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsNOr = dadi.Spectrum.from_phi(phiNr, (n1,n2), (xx,xx))
    fsNMr = dadi.Numerics.reverse_array(fsNOr)

    # Barrier spectrum
    # Spectrum of non-recombining regions
    phiInr = dadi.PhiManip.phi_1D(xx)
    phiInr = dadi.PhiManip.phi_1D_to_2D(xx, phiInr)
    phiInr = dadi.Integration.two_pops(phiInr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    phiInr = dadi.Integration.two_pops(phiInr, xx, T=Ti, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    fsIOnr = dadi.Spectrum.from_phi(phiInr, (n1,n2), (xx,xx))
    fsIMnr = dadi.Numerics.reverse_array(fsIOnr)
    # Spectrum of recombining regions
    phiIr = dadi.PhiManip.phi_1D(xx)
    phiIr = dadi.PhiManip.phi_1D_to_2D(xx, phiIr)
    phiIr = dadi.Integration.two_pops(phiIr, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    phiIr = dadi.Integration.two_pops(phiIr, xx, T=Ti, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsIOr = dadi.Spectrum.from_phi(phiIr, (n1,n2), (xx,xx))
    fsIMr = dadi.Numerics.reverse_array(fsIOr)

    # Sum all spectra
    fs = O*( P*nr*fsNOnr + P*(1-nr)*fsNOr + (1-P)*nr*fsIOnr + (1-P)*(1-nr)*fsIOr ) + (1-O)*( P*nr*fsNMnr + P*(1-nr)*fsNMr + (1-P)*nr*fsIMnr + (1-P)*(1-nr)*fsIMr )
    return fs


def AM2N2M2P(params, ns, pts):
    nu1, nu2, m12, m21, Ts, Ti, nr, bf, P1, P2, O = params
    n1,n2 = ns
    """
    Ancient Migration with heterogeneous Ne (shared between populations) and heterogeneous Me (population-specific).

    nu1: Size of population 1 after split.
    nu2: Size of population 2 after split.
    m12: Migration from pop 2 to pop 1 in neutral regions. In barrier regions, m12=0.
    m21: Migration from pop 1 to pop 2 in neutral regions. In barrier regions, m21=0.
    Ts: Duration of divergence with migration.
    Ti: Duration of divergence in isolation.
    nr: Fraction of non-recombining regions, i.e. with reduced Ne.
    bf : Background factor that defines to which extent Ne is reduced.
    P1: Fraction of the genome evolving neutrally in population 1.
    P2: Fraction of the genome evolving neutrally in population 2.
    O: Fraction of SNPs accurately orientated.
    n1,n2: Size of fs to generate.
    pts: Number of points to use in grid for evaluation.
    """
    # Define the grid we'll use
    xx = dadi.Numerics.default_grid(pts)

    # Neutral spectrum in population 1 and 2
    # Spectrum of non-recombining regions
    phiN1N2nr = dadi.PhiManip.phi_1D(xx)
    phiN1N2nr = dadi.PhiManip.phi_1D_to_2D(xx, phiN1N2nr)
    phiN1N2nr = dadi.Integration.two_pops(phiN1N2nr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=m12, m21=m21)
    phiN1N2nr = dadi.Integration.two_pops(phiN1N2nr, xx, T=Ti, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    fsN1N2Onr = dadi.Spectrum.from_phi(phiN1N2nr, (n1,n2), (xx,xx))
    fsN1N2Mnr = dadi.Numerics.reverse_array(fsN1N2Onr)
    # Spectrum of recombining regions
    phiN1N2r = dadi.PhiManip.phi_1D(xx)
    phiN1N2r = dadi.PhiManip.phi_1D_to_2D(xx, phiN1N2r)
    phiN1N2r = dadi.Integration.two_pops(phiN1N2r, xx, T=Ts, nu1=nu1, nu2=nu2, m12=m12, m21=m21)
    phiN1N2r = dadi.Integration.two_pops(phiN1N2r, xx, T=Ti, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsN1N2Or = dadi.Spectrum.from_phi(phiN1N2r, (n1,n2), (xx,xx))
    fsN1N2Mr = dadi.Numerics.reverse_array(fsN1N2Or)

    # Barrier spectrum in population 1 and 2
    # Spectrum of non-recombining regions
    phiI1I2nr = dadi.PhiManip.phi_1D(xx)
    phiI1I2nr = dadi.PhiManip.phi_1D_to_2D(xx, phiI1I2nr)
    phiI1I2nr = dadi.Integration.two_pops(phiI1I2nr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    phiI1I2nr = dadi.Integration.two_pops(phiI1I2nr, xx, T=Ti, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    fsI1I2Onr = dadi.Spectrum.from_phi(phiI1I2nr, (n1,n2), (xx,xx))
    fsI1I2Mnr = dadi.Numerics.reverse_array(fsI1I2Onr)
    # Spectrum of recombining regions
    phiI1I2r = dadi.PhiManip.phi_1D(xx)
    phiI1I2r = dadi.PhiManip.phi_1D_to_2D(xx, phiI1I2r)
    phiI1I2r = dadi.Integration.two_pops(phiI1I2r, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=0)
    phiI1I2r = dadi.Integration.two_pops(phiI1I2r, xx, T=Ti, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsI1I2Or = dadi.Spectrum.from_phi(phiI1I2r, (n1,n2), (xx,xx))
    fsI1I2Mr = dadi.Numerics.reverse_array(fsI1I2Or)

    # Neutral spectrum in population 1, barrier spectrum in population 2
    # Spectrum of non-recombining regions
    phiN1I2nr = dadi.PhiManip.phi_1D(xx)
    phiN1I2nr = dadi.PhiManip.phi_1D_to_2D(xx, phiN1I2nr)
    phiN1I2nr = dadi.Integration.two_pops(phiN1I2nr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=m12, m21=0)
    phiN1I2nr = dadi.Integration.two_pops(phiN1I2nr, xx, T=Ti, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    fsN1I2Onr = dadi.Spectrum.from_phi(phiN1I2nr, (n1,n2), (xx,xx))
    fsN1I2Mnr = dadi.Numerics.reverse_array(fsN1I2Onr)
    # Spectrum of recombining regions
    phiN1I2r = dadi.PhiManip.phi_1D(xx)
    phiN1I2r = dadi.PhiManip.phi_1D_to_2D(xx, phiN1I2r)
    phiN1I2r = dadi.Integration.two_pops(phiN1I2r, xx, T=Ts, nu1=nu1, nu2=nu2, m12=m12, m21=0)
    phiN1I2r = dadi.Integration.two_pops(phiN1I2r, xx, T=Ti, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsN1I2Or = dadi.Spectrum.from_phi(phiN1I2r, (n1,n2), (xx,xx))
    fsN1I2Mr = dadi.Numerics.reverse_array(fsN1I2Or)

    # Neutral spectrum in population 2, barrier spectrum in population 1
    # Spectrum of non-recombining regions
    phiI1N2nr = dadi.PhiManip.phi_1D(xx)
    phiI1N2nr = dadi.PhiManip.phi_1D_to_2D(xx, phiI1N2nr)
    phiI1N2nr = dadi.Integration.two_pops(phiI1N2nr, xx, T=Ts, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=m21)
    phiI1N2nr = dadi.Integration.two_pops(phiI1N2nr, xx, T=Ti, nu1=nu1*bf, nu2=nu2*bf, m12=0, m21=0)
    fsI1N2Onr = dadi.Spectrum.from_phi(phiI1N2nr, (n1,n2), (xx,xx))
    fsI1N2Mnr = dadi.Numerics.reverse_array(fsI1N2Onr)
    # Spectrum of recombining regions
    phiI1N2r = dadi.PhiManip.phi_1D(xx)
    phiI1N2r = dadi.PhiManip.phi_1D_to_2D(xx, phiI1N2r)
    phiI1N2r = dadi.Integration.two_pops(phiI1N2r, xx, T=Ts, nu1=nu1, nu2=nu2, m12=0, m21=m21)
    phiI1N2r = dadi.Integration.two_pops(phiI1N2r, xx, T=Ti, nu1=nu1, nu2=nu2, m12=0, m21=0)
    fsI1N2Or = dadi.Spectrum.from_phi(phiI1N2r, (n1,n2), (xx,xx))
    fsI1N2Mr = dadi.Numerics.reverse_array(fsI1N2Or)

    # Sum all spectra
    fs = O*( P1*P2*nr*fsN1N2Onr + P1*P2*(1-nr)*fsN1N2Or + (1-P1)*(1-P2)*nr*fsI1I2Onr + (1-P1)*(1-P2)*(1-nr)*fsI1I2Or + P1*(1-P2)*nr*fsN1I2Onr + P1*(1-P2)*(1-nr)*fsN1I2Or + (1-P1)*P2*nr*fsI1N2Onr + (1-P1)*P2*(1-nr)*fsI1N2Or ) + (1-O)*( P1*P2*nr*fsN1N2Mnr + P1*P2*(1-nr)*fsN1N2Mr + (1-P1)*(1-P2)*nr*fsI1I2Mnr + (1-P1)*(1-P2)*(1-nr)*fsI1I2Mr + P1*(1-P2)*nr*fsN1I2Mnr + P1*(1-P2)*(1-nr)*fsN1I2Mr + (1-P1)*P2*nr*fsI1N2Mnr + (1-P1)*P2*(1-nr)*fsI1N2Mr )
    return fs
