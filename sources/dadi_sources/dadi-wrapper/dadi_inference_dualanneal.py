#!/usr/local/bin/python
# -*- coding: utf-8 -*-

######################################################################################################################################################
#								IMPORT PACKAGES
######################################################################################################################################################
import os
import sys
import time
import getopt
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import pylab
import numpy
from numpy import array
from math import log
sys.path.append('~/demographic_inferences/source/dadi-2.0.6_modif/dadi') #local path to python packages where dadi is installed
from scipy import stats
import dadi
import dadi_models_2pop


######################################################################################################################################################
#								DEFINE FUNCTIONS
######################################################################################################################################################
##### Help function
def usage():
	print("# Minimal command to run 20 replicates: for i in $(seq 1 1 20); do python dadi_inference_dualanneal.py -i path_to_input_name > log-${i}.txt &; done\n"+
	      "# Output files are: 1) full output 2) easy-to-handle output 3) model sfs. The full output contains the inference setting, the summary statistics, starting and final logL, AIC, theta and parameter array. Note that optimization is performed with the 'the dual annealing algorithm in which the  global optimization is set to be a classic annealing algorithm and the local optimisation is BFGS (i.e. quasi-Newton).\n"+
	      "# -h --help: Display this help.\n"+
	      "# -v --verbose: If set, print intermediate optimization steps on stdout (best logL, current step logL, parameter array). [False]\n"+
	      "# -z --masked: If set, mask the singletons in the sfs: [1,0] and [0,1]. [False]\n"+
	      "# -d --folded: If set, the sfs is folded (i.e. no outgroup to polarize the data). [False]\n"+
	      "# -j --projected: If set, the sfs is projected down to n1,n2 (values to be set with option '-n'). If your data have missing calls for some individuals, projecting down to a smaller sample size will increase the number of SNPs you can use. It will average over all possible re-samplings of the larger sample size data (assuming random mating). [False]\n"+
	      "# -o --nameoutput: Prefix for output files. [dadi]\n"+
	      "# -i --fs_file_name: Path-name to the input.\n"+
	      "# -c --pop_file_name: Path-name to the file describing how individuals map to populations. This file is ignored if datatype is not 'vcf'. [None]\n"+
	      "# -t --datatype: Type of input: 1) simul: ms simulation. 2) sfs: sfs data. 3) snp-dadi: snp data with dadi format; the sample size is required (values to be set with options '-j' and '-n'). 4) vcf: vcf data; it requires '-c' to be provided and it will only include sites without missing data. [sfs]\n"+
	      "# -y --namepop1: Name of population 1 in the sfs (y-axis). [Pop1]\n"+
	      "# -x --namepop2: Name of population 2 in the sfs (x-axis). [Pop2]\n"+
	      "# -n --proj_sample: Sample size of population 1 and 2 in the sfs. Recall that for a diploid organism we get two samples from each individual. These values are ignored if '-j' is not set. [10,10]\n"+
	      "# -p --grid_points: Size of the grids for extrapolation. dadi solves a partial differential equation, approximating the solution using a grid of points in population frequency space (diffusion approximation). The grid takes 3 numbers separated by a coma. Good results are obtained by setting the smallest grid size slightly larger than the largest sample size, but this can be altered depending on usage. If you are fitting a complex model, it may speed up the analysis considerably to first run an optimization at small grid sizes. They can then be refined by running another optimization with a finer grid. [nMax,nMax*2,nMax*3]\n"+
	      "# -m --model_list: List the models to include in the inference. All available models are defined in 'dadi_models_2pop.py'. [SI,IM,SC,AM]\n\n\n")
	return()


##### Function that records the arguments from the command line
def takearg(argv):
	# default values
	verbose = False
	masked = False
	folded = False
	projected = False
	checkfile = False
	nameoutput = "dadi"
	pop_file_name = None
	datatype = "sfs"
	namepop1 = "Pop1"
	namepop2 = "Pop2"
	proj_sample = [10,10]
	pts_l = None
	model_list = ["SI", "IM", "SC", "AM"]

	# check values
	if len(argv) < 2:
		print("You should give, at least, the name of the sfs file !")
		sys.exit(1)
	try:
		opts, args = getopt.getopt(sys.argv[1:], "hvzdjo:i:c:t:y:x:n:p:m:", ["help", "verbose", "masked", "folded", "projected", "nameoutput=", "fs_file_name=", "pop_file_name=", "datatype=", "namepop1=", "namepop2=", "proj_sample=", "grid_points=", "model_list="])
	except getopt.GetoptError as err:
		print(err)
		usage()
		sys.exit(2)

	# extract values from command line, or set to "True" if boolean is absent from command line.
	for opt, arg in opts:
		if opt in ("-h", "--help"):
			usage()
			sys.exit()
		elif opt in ("-v", "--verbose"):
			verbose = True
		elif opt in ("-z", "--masked"):
			masked = True
		elif opt in ("-d", "--folded"):
			folded = True
		elif opt in ("-j", "--projected"):
			projected = True
		elif opt in ("-o", "--nameoutput"):
			nameoutput = arg
		elif opt in ("-i", "--fs_file_name"):
			fs_file_name = arg
			checkfile = True
		elif opt in ("-c", "--pop_file_name"):
			pop_file_name = arg
		elif opt in ("-t", "--datatype"):
			datatype = arg
		elif opt in ("-y", "--namepop1"):
			namepop1 = arg
		elif opt in ("-x", "--namepop2"):
			namepop2 = arg
		elif opt in ("-n", "--proj_sample"):
			proj_sample = arg.split(",")
		elif opt in ("-p", "--grid_points"):
			pts_l = arg.split(",")
		elif opt in ("-m", "--model_list"):
			model_list = arg.split(",")
		else:
			print("Option {} inconnue".format(opt))
			sys.exit(2)
	if not checkfile:
		print("You should give, at least, the name of the sfs file !")
		sys.exit(1)

	# return values
	return(verbose, masked, folded, projected, nameoutput, fs_file_name, pop_file_name, datatype, namepop1, namepop2, proj_sample, pts_l, model_list)


##### Saturation function
def lnLmax(fs,ns):
	lnL = 0
	imax = ns[0]-1
	jmax = ns[1]-1
	for i in range(0,imax):
		for j in range(0,jmax):
			k = fs[i,j]
			if k > 0:
				#fs[i,j] is Poisson distributed (Sylvain Glemin)
				lnL += -k + k*log(k) - numpy.math.lgamma(k+1)
	return(lnL)


##### Inference function
def callmodel(func, data, pts_l=None, ns=None, modeldemo=None, namepop1=None, namepop2=None,
	      params=None, fixed_params=None, lower_bound=None, upper_bound=None,
	      maxiterGlobal=100, accept=1, visit=1.01, Tini=50,
	      no_local_search=False, maxiterLocal=100, local_method='BFGS',
	      output_file="output-1", output_file_2="output-2", output_file_3="output-3",
	      nameoutput=None, nameoutput2=None, verbose=True, full_output=True):

	# Extrapolate the model function.
	func_ex = dadi.Numerics.make_extrap_log_func(func)

	# Calculate the expected model SFS.
	model = func_ex(params, ns, pts_l)

	# Likelihood of the data given the model SFS.
	ll_model = dadi.Inference.ll_multinom(model, data)

	# The optimal value of theta (4*Nref*mu*L) given the model.
	theta = dadi.Inference.optimal_sfs_scaling(model, data)

	# Calculate the expected model SFS with upper_bound params.
	# And then decide to change the bounds or not based on the execution time or the presence of warnings.
	# if verbose == True:
	# 	start_time = time.time()
	# 	model = func_ex(upper_bound, ns, pts_l)
	# 	sfstime = time.time() - start_time
	# 	ll_model = dadi.Inference.ll_multinom(model, data)
	# 	print(0, ll_model, upper_bound, sfstime, sep=" , " )

	# Do the optimization.
	# Global followed by local (BFGS) searchs. It requires 'full_output=True' to return the maxlogL.
	if optimizationstate == "dual_anneal":
	
		# Perturb the parameter array before optimization by taking each parameter up to a <fold> factor of 2 up or down. 
		# This is required to generate a new initial point for the optimization between replicates.
		p0 = dadi.Misc.perturb_params(params, fold=1, lower_bound=lower_bound, upper_bound=upper_bound)	

		popt = dadi.Inference.optimize_dual_anneal(p0=p0, data=data, model_func=func_ex, pts=pts_l,
                                                     lower_bound=lower_bound, upper_bound=upper_bound,
                                                     no_local_search=no_local_search, local_method=local_method, local_maxiter=maxiterLocal,
                                                     maxiter=maxiterGlobal, Tini=Tini, accept=accept, visit=visit, verbose=verbose, full_output=True)
		if full_output:
			popt = popt.xmax

	# Extract results.
	model = func_ex(popt, ns, pts_l)
	ll_opt = dadi.Inference.ll_multinom(model, data)
	theta_opt = dadi.Inference.optimal_sfs_scaling(model, data)
	AIC = 2*len(params)-2*ll_opt
	
	# Write results.
	line = (str(modeldemo) + "\n" + "Starting log-likelihood: " + repr(ll_model) + "\n" + "Starting theta: " + repr(theta) + "\n" + "Starting parameters: " + ';'.join(map(str, p0)) + "\n\n" "Optimization: " + repr(optimizationstate) + "\n" + "AIC: " + repr(AIC) + "\n" + "Optimized log-likelihood: " + repr(ll_opt) + "\n" + "Optimized theta: " + repr(theta_opt) + "\n" + "Optimized parameters: " + ';'.join(map(str, popt)) + "\n")
	output_file.write(line)

	# Write results at the end of the optimization.
	if optimizationstate == "dual_anneal":
		#Plot SFS: data, model, residuals.
		fig = pylab.figure(4,figsize=(10,8))
		fig.clear()
		dadi.Plotting.plot_2d_comp_multinom(model, data, vmin=1, pop_ids =(namepop1,namepop2), residual='Anscombe', show=False)
		fig.savefig(nameoutput + "_" + modeldemo + ".png")

		#Extract best logL and AIC for each run.
		line = (str(nameoutput2) + "\t" + repr(ll_opt)+ "\t" + repr(AIC) + "\n")
		output_file_2.write(line)

		#Extract model SFS in a file.
		model_sfs = dadi.Inference.optimally_scaled_sfs(model, data)
		for i in range(1,len(model_sfs)-1):
			output_file_3.write(str(model_sfs[i]) + '\n')

	done=True
	return(done, popt)


######################################################################################################################################################
#								LOADINGS, SETTINGS, STATISTICS
######################################################################################################################################################
##### Load parameters from the command line
verbose, masked, folded, projected, nameoutput, fs_file_name, pop_file_name, datatype, namepop1, namepop2, proj_sample, pts_l, model_list = takearg(sys.argv)

##### Load data
datastate = "not_folded"
if datatype == "simul":
	if folded == True:
		print(("\nnPolarization is knwown in ms simulations !\n"))
		datastate = "folded"
	else:
		data = dadi.Spectrum.from_ms_file(fs_file_name, average=False)
		ns = data.sample_sizes
elif datatype == "sfs":
	data = dadi.Spectrum.from_file(fs_file_name)
	if folded == True:
		data = data.fold()
		datastate = "folded"
	ns = data.sample_sizes
elif datatype == "snp-dadi":
	dd = dadi.Misc.make_data_dict(fs_file_name)
	if projected == True:
		if folded == True:
			data = dadi.Spectrum.from_data_dict(dd, pop_id=[namepop1,namepop2], projections=[proj_sample[0],proj_sample[1]], polarized=False)
			datastate = "folded"
		else:
			data = dadi.Spectrum.from_data_dict(dd, pop_id=[namepop1,namepop2], projections=[proj_sample[0],proj_sample[1]], polarized=True)
	else:
		print(("\nYou should set '-p' and specify the sample sizes with '-n' !\n"))
	ns = data.sample_sizes
else:
	dd = dadi.Misc.make_data_dict_vcf(fs_file_name, pop_file_name)
	if projected == True:
		if folded == True:
			data = dadi.Spectrum.from_data_dict(dd, pop_id=[namepop1,namepop2], projections=[proj_sample[0],proj_sample[1]], polarized=False)
			datastate = "folded"
		else:
			print(("\nPolarized data is not implemented in the 'import from VCF' module !\n"))
	else:
		print(("\nYou should set '-p' and specify the sample sizes with '-n' !\n"))
	ns = data.sample_sizes

##### Set singleton masking
datastate2 = "not_masked"
if masked == True:
	data.mask[1,0] = True
	data.mask[0,1] = True
	datastate2 = "masked"

##### Set down-projection
datastate3 = "not_projdown"
if projected == True:
	data = data.project([proj_sample[0],proj_sample[1]])
	datastate3 = "projdown"

##### Set grid sizes
if pts_l != None:
	for i in range(len(pts_l)):
		pts_l[i] = int(pts_l[i])
else:
	pts_l = [max(ns),max(ns)*2,max(ns)*3]

##### Set optimization algorithm
opt_list = ["dual_anneal"]

##### Set prefix
# uncomment next line for original behaviour
#nameoutput = nameoutput + "_" + repr(time.localtime()[0]) + repr(time.localtime()[1]) + repr(time.localtime()[2]) + repr(time.localtime()[3]) + repr(time.localtime()[4]) + repr(time.localtime()[5])

##### Set outputs
if not os.path.isdir(nameoutput): # precaution as snakemake creates dir automatically
	os.mkdir(nameoutput)
output_file = open((nameoutput + "/" + nameoutput + "-full.txt"), "w")
output_file_2 = open((nameoutput + "/" + nameoutput + "-easy.txt"), "w")
output_file_3 = open((nameoutput + "/" + nameoutput + "-modelsfs.txt"), "w")


##### Calculate statistics from the data
# Likelihood of the saturated model
ll_max = lnLmax(data, ns)

# Total number of segregating sites S
S = data.S()

# Wright's FST by the method of Weir and Cockerham.
Fst = round(data.Fst(),3)

# One-pop statistics
data1 = data.marginalize([1])
data2 = data.marginalize([0])
S1 = data1.S()
S2 = data2.S()

# Watterson's theta
thetaW1 = round(data1.Watterson_theta(),0)
thetaW2 = round(data2.Watterson_theta(),0)

# Expected heterozygosity pi assuming random mating
pi1 = round(data1.pi(),0)
pi2 = round(data2.pi(),0)

# Tajima's D
D1 = round(data1.Tajima_D(),3)
D2 = round(data2.Tajima_D(),3)

# Write-down inference setting
line = ("Model(s): " + repr(model_list) + "; Optimization(s): " + repr(opt_list) + "\n" + "Data type: " + repr(datatype) + "; Pop1: " + repr(namepop1) + " of sample size: " + repr(ns[0]) + "; Pop2: " + repr(namepop2) + " of sample size: " + repr(ns[1]) + "\n" + "SFS: " + repr(datastate) + "; Singletons: " + repr(datastate2) + "; Down-projection: " + repr(datastate3) + "; Grid size: " + repr(pts_l) + "\n" + "Number of seg. sites S1: " + repr(S1) + "; Watterson's Theta1: " + repr(thetaW1) + "; Expected heterozygosity pi1: " + repr(pi1) + "; Tajima's D1: " + repr(D1) + "\n" + "Number of seg. sites S2: " + repr(S2) + "; Watterson's Theta2: " + repr(thetaW2) + "; Expected heterozygosity pi2: " + repr(pi2) + "; Tajima's D2: " + repr(D2) + "\n" + "Weir & Cockerham's Fst: " + repr(Fst) + "; Total number of seg. sites S: " + repr(S) + "\n" + "Likelihood saturated model: " + repr(ll_max) + "\n\n\n")
output_file.write(line)


######################################################################################################################################################
#								PERFORM THE INFERENCE
######################################################################################################################################################
##### Set optimization values
maxiterGlobal=100 #maximum global search iterations - in each iteration, it explores twice the number of parameters
accept=1 #parameter for acceptance distribution (lower values makes the probability of acceptance smaller)
visit=1.01 #parameter for visiting distribution (higher values makes the algorithm jump to a more distant region)
Tini=50 #initial temperature
no_local_search=False #If set to True, a Generalized Simulated Annealing will be performed with no local search strategy applied
local_method='L-BFGS-B' #local search method
maxiterLocal=20 #maximum local search iterations

##### Set bounds and starting values
# If fits often push the bounds of the parameter space, this indicates 1) that bounds are too conservative. 2) that the model is misspecified.
# It is often useful to optimize only a subset of model parameters, and so to fix others with 'fixed_params'.
n1_max = 20.0 ; n1_min = 1e-3 ; n1_start = 1.0 #current size of pop1
n2_max = 20.0 ; n2_min = 1e-3 ; n2_start = 1.0 #current size of pop2
ne1_max = 20.0 ; ne1_min = 1e-3 ; ne1_start = 1.0 #size of pop1 after exponential change
ne2_max = 20.0 ; ne2_min = 1e-3 ; ne2_start = 1.0 #size of pop2 after exponential change
Ts_max = 12.0 ; Ts_min = 1e-10 ; Ts_start = 1.0 #duration of the period after split
Ti_max = 12.0 ; Ti_min = 1e-10 ; Ti_start = 1.0 #duration of the second period
m12_max = 20.0 ; m12_min = 1e-10 ; m12_start = 1.0 #migration from 2 to 1
m21_max = 20.0 ; m21_min = 1e-10 ; m21_start = 1.0 #migration from 1 to 2
nr_max = 1.00 ; nr_min = 1e-10 ; nr_start = 0.5 #fraction of the genome with reduced Ne
bf_max = 1.00 ; bf_min = 1e-3 ; bf_start = 0.5 #factor of Ne reduction
P_max = 1.00 ; P_min = 1e-10 ; P_start = 0.5 #1 - fraction of the genome with Me=0
O_max = 1.0 ; O_min = 1e-10 ; O_start=0.8 #fraction of SNPs accurately oriented relative to the outroup

##### Inference for each model in the list
for namemodel in model_list:
	print(namemodel)
	time.sleep(1.0)

	# Strict Isolation
	# nu1, nu2, Ts, O
	if namemodel == "SI":
		func = dadi_models_2pop.SI

		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [n1_start, n2_start, Ts_start, O_start]

			upper_bound = [n1_max, n2_max, Ts_max, O_max]
			lower_bound = [n1_min, n2_min, Ts_min, O_min]
			
			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)

		if done: print(("\n" + namemodel + " : done\n"))

	# Strict Isolation with exponential size changes
	# nu1e, nu1, nu2e, nu2, Ts, Ti, O
	if namemodel == "SIexp":
		func = dadi_models_2pop.SIexp

		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [ne1_start, n1_start, ne2_start, n2_start, Ts_start, Ti_start, O_start]

			upper_bound = [ne1_max, n1_max, ne2_max, n2_max, Ts_max, Ti_max, O_max]
			lower_bound = [ne1_min, n1_min, ne2_min, n2_min, Ts_min, Ti_min, O_min]
			
			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)

		if done: print(("\n" + namemodel + " : done\n"))

	# Strict Isolation with heterogeneous Ne (shared between populations)
	# nu1, nu2, Ts, nr, bf, O
	if namemodel == "SI2N":
		func = dadi_models_2pop.SI2N

		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [n1_start, n2_start, Ts_start, nr_start, bf_start, O_start]

			upper_bound = [n1_max, n2_max, Ts_max, nr_max, bf_max, O_max]
			lower_bound = [n1_min, n2_min, Ts_min, nr_min, bf_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)

		if done: print(("\n" + namemodel + " : done\n"))

	# Isolation with Migration
	# nu1, nu2, m12, m21, Ts, O
	if namemodel == "IM":
		func = dadi_models_2pop.IM

		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [n1_start, n2_start, m12_start, m21_start, Ts_start, O_start]
		
			upper_bound = [n1_max, n2_max, m12_max, m21_max, Ts_max, O_max]
			lower_bound = [n1_min, n2_min, m12_min, m21_min, Ts_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)
		if done: print(("\n" + namemodel + " : done\n"))

	# Isolation with Migration with exponential size changes
	# nu1e, nu1, nu2e, nu2, m12, m21, Ts, Ti, O
	if namemodel == "IMexp":
		func = dadi_models_2pop.IMexp

		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [ne1_start, n1_start, ne2_start, n2_start, m12_start, m21_start, Ts_start, Ti_start, O_start]
		
			upper_bound = [ne1_max, n1_max, ne2_max, n2_max, m12_max, m21_max, Ts_max, Ti_max, O_max]
			lower_bound = [ne1_min, n1_min, ne2_min, n2_min, m12_min, m21_min, Ts_min, Ti_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)
		if done: print(("\n" + namemodel + " : done\n"))

	# Isolation with Migration with heterogeneous Ne (shared between populations)
	# nu1, nu2, m12, m21, Ts, nr, bf, O
	if namemodel == "IM2N":
		func = dadi_models_2pop.IM2N

		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [n1_start, n2_start, m12_start, m21_start, Ts_start, nr_start, bf_start, O_start]

			upper_bound = [n1_max, n2_max, m12_max, m21_max, Ts_max, nr_max, bf_max, O_max]
			lower_bound = [n1_min, n2_min, m12_min, m21_min, Ts_min, nr_min, bf_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)

		if done: print(("\n" + namemodel + " : done\n"))

	# Isolation with Migration with heterogeneous Me (shared between populations)
	# nu1, nu2, m12, m21, Ts, P, O
	if namemodel == "IM2M":
		func = dadi_models_2pop.IM2M

		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [n1_start, n2_start, m12_start, m21_start, Ts_start, P_start, O_start]
				
			upper_bound = [n1_max, n2_max, m12_max, m21_max, Ts_max, P_max, O_max]
			lower_bound = [n1_min, n2_min, m12_min, m21_min, Ts_min, P_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)

		if done: print(("\n" + namemodel + " : done\n"))

	# Isolation with Migration with heterogeneous Me (population-specific)
	# nu1, nu2, m12, m21, Ts, P1, P2, O
	if namemodel == "IM2M2P":
		func = dadi_models_2pop.IM2M2P

		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [n1_start, n2_start, m12_start, m21_start, Ts_start, P_start, P_start, O_start]
		
			upper_bound = [n1_max, n2_max, m12_max, m21_max, Ts_max, P_max, P_max, O_max]
			lower_bound = [n1_min, n2_min, m12_min, m21_min, Ts_min, P_min, P_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)

		if done: print(("\n" + namemodel + " : done\n"))

	# Isolation with Migration with heterogeneous Ne (shared between populations) and heterogeneous Me (shared between populations)
	# nu1, nu2, m12, m21, Ts, nr, bf, P, O
	if namemodel == "IM2N2M":
		func = dadi_models_2pop.IM2N2M

		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [n1_start, n2_start, m12_start, m21_start, Ts_start, nr_start, bf_start, P_start, O_start]
				
			upper_bound = [n1_max, n2_max, m12_max, m21_max, Ts_max, nr_max, bf_max, P_max, O_max]
			lower_bound = [n1_min, n2_min, m12_min, m21_min, Ts_min, nr_min, bf_min, P_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)

		if done: print(("\n" + namemodel + " : done\n"))

	# Isolation with Migration with heterogeneous Ne (shared between populations) and heterogeneous Me (population-specific)
	# nu1, nu2, m12, m21, Ts, nr, bf, P1, P2, O
	if namemodel == "IM2N2M2P":
		func = dadi_models_2pop.IM2N2M2P

		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [n1_start, n2_start, m12_start, m21_start, Ts_start, nr_start, bf_start, P_start, P_start, O_start]
		
			upper_bound = [n1_max, n2_max, m12_max, m21_max, Ts_max, nr_max, bf_max, P_max, P_max, O_max]
			lower_bound = [n1_min, n2_min, m12_min, m21_min, Ts_min, nr_min, bf_min, P_min, P_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)

		if done: print(("\n" + namemodel + " : done\n"))

	# Secondary Contact
	# nu1, nu2, m12, m21, Ts, Ti, O
	if namemodel == "SC":
		func = dadi_models_2pop.SC
		
		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [n1_start, n2_start, m12_start, m21_start, Ts_start, Ti_start, O_start]
		
			upper_bound = [n1_max, n2_max, m12_max, m21_max, Ts_max, Ti_max, O_max]
			lower_bound = [n1_min, n2_min, m12_min, m21_min, Ts_min, Ti_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)

		if done: print(("\n" + namemodel + " : done\n"))

	# Secondary Contact with exponential size changes
	# nu1e, nu1, nu2e, nu2, m12, m21, Ts, Ti, O
	if namemodel == "SCexp":
		func = dadi_models_2pop.SCexp
		
		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [ne1_start, n1_start, ne2_start, n2_start, m12_start, m21_start, Ts_start, Ti_start, O_start]
		
			upper_bound = [ne1_max, n1_max, ne2_max, n2_max, m12_max, m21_max, Ts_max, Ti_max, O_max]
			lower_bound = [ne1_min, n1_min, ne2_min, n2_min, m12_min, m21_min, Ts_min, Ti_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)
		if done: print(("\n" + namemodel + " : done\n"))

	# Secondary Contact with heterogeneous Ne (shared between populations)
	# nu1, nu2, m12, m21, Ts, Ti, nr, bf, O
	if namemodel == "SC2N":
		func = dadi_models_2pop.SC2N

		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [n1_start, n2_start, m12_start, m21_start, Ts_start, Ti_start, nr_start, bf_start, O_start]

			upper_bound = [n1_max, n2_max, m12_max, m21_max, Ts_max, Ti_max, nr_max, bf_max, O_max]
			lower_bound = [n1_min, n2_min, m12_min, m21_min, Ts_min, Ti_min, nr_min, bf_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)

		if done: print(("\n" + namemodel + " : done\n"))

	# Secondary Contact with heterogeneous Me (shared between populations)
	# nu1, nu2, m12, m21, Ts, Ti, P, O
	if namemodel == "SC2M":
		func = dadi_models_2pop.SC2M

		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [n1_start, n2_start, m12_start, m21_start, Ts_start, Ti_start, P_start, O_start]

			upper_bound = [n1_max, n2_max, m12_max, m21_max, Ts_max, Ti_max, P_max, O_max]
			lower_bound = [n1_min, n2_min, m12_min, m21_min, Ts_min, Ti_min, P_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)

		if done: print(("\n" + namemodel + " : done\n"))

	# Secondary Contact with heterogeneous Me (population-specific)
	# nu1, nu2, m12, m21, Ts, Ti, P1, P2, O
	if namemodel == "SC2M2P":
		func = dadi_models_2pop.SC2M2P

		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [n1_start, n2_start, m12_start, m21_start, Ts_start, Ti_start, P_start, P_start, O_start]

			upper_bound = [n1_max, n2_max, m12_max, m21_max, Ts_max, Ti_max, P_max, P_max, O_max]
			lower_bound = [n1_min, n2_min, m12_min, m21_min, Ts_min, Ti_min, P_min, P_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)

		if done: print(("\n" + namemodel + " : done\n"))

	# Secondary Contact with heterogeneous Ne (shared between populations) and heterogeneous Me (shared between populations): 
	# nu1, nu2, m12, m21, Ts, Ti, nr, bf, P, O
	if namemodel == "SC2N2M":
		func = dadi_models_2pop.SC2N2M

		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [n1_start, n2_start, m12_start, m21_start, Ts_start, Ti_start, nr_start, bf_start, P_start, O_start]
				
			upper_bound = [n1_max, n2_max, m12_max, m21_max, Ts_max, Ti_max, nr_max, bf_max, P_max, O_max]
			lower_bound = [n1_min, n2_min, m12_min, m21_min, Ts_min, Ti_min, nr_min, bf_min, P_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)

		if done: print(("\n" + namemodel + " : done\n"))

	# Secondary Contact with heterogeneous Ne (shared between populations) and heterogeneous Me (population-specific)
	# nu1, nu2, m12, m21, Ts, Ti, nr, bf, P1, P2, O
	if namemodel == "SC2N2M2P":
		func = dadi_models_2pop.SC2N2M2P

		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [n1_start, n2_start, m12_start, m21_start, Ts_start, Ti_start, nr_start, bf_start, P_start, P_start, O_start]
		
			upper_bound = [n1_max, n2_max, m12_max, m21_max, Ts_max, Ti_max, nr_max, bf_max, P_max, P_max, O_max]
			lower_bound = [n1_min, n2_min, m12_min, m21_min, Ts_min, Ti_min, nr_min, bf_min, P_min, P_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)

		if done: print(("\n" + namemodel + " : done\n"))

	# Ancient Migration
	# nu1, nu2, m12, m21, Ts, Ti, O
	if namemodel == "AM":
		func = dadi_models_2pop.AM

		for optimizationstate in opt_list:
			print(optimizationstate)
		
			if optimizationstate == "dual_anneal":
				params = [n1_start, n2_start, m12_start, m21_start, Ts_start, Ti_start, O_start]
		
			upper_bound = [n1_max, n2_max, m12_max, m21_max, Ts_max, Ti_max, O_max]
			lower_bound = [n1_min, n2_min, m12_min, m21_min, Ts_min, Ti_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)

		if done: print(("\n" + namemodel + " : done\n"))

	# Ancient Migration with exponential size changes
	# nu1e, nu1, nu2e, nu2, m12, m21, Ts, Ti, O
	if namemodel == "AMexp":
		func = dadi_models_2pop.AMexp
		
		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [ne1_start, n1_start, ne2_start, n2_start, m12_start, m21_start, Ts_start, Ti_start, O_start]
		
			upper_bound = [ne1_max, n1_max, ne2_max, n2_max, m12_max, m21_max, Ts_max, Ti_max, O_max]
			lower_bound = [ne1_min, n1_min, ne2_min, n2_min, m12_min, m21_min, Ts_min, Ti_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)
		if done: print(("\n" + namemodel + " : done\n"))

	# Ancient Migration with heterogeneous Ne (shared between populations)
	# nu1, nu2, m12, m21, Ts, Ti, nr, bf, O
	if namemodel == "AM2N":
		func = dadi_models_2pop.AM2N

		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [n1_start, n2_start, m12_start, m21_start, Ts_start, Ti_start, nr_start, bf_start, O_start]

			upper_bound = [n1_max, n2_max, m12_max, m21_max, Ts_max, Ti_max, nr_max, bf_max, O_max]
			lower_bound = [n1_min, n2_min, m12_min, m21_min, Ts_min, Ti_min, nr_min, bf_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)

		if done: print(("\n" + namemodel + " : done\n"))

	# Ancient Migration with heterogeneous Me (shared between populations)
	# nu1, nu2, m12, m21, Ts, Ti, P, O
	if namemodel == "AM2M":
		func = dadi_models_2pop.AM2M

		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [n1_start, n2_start, m12_start, m21_start, Ts_start, Ti_start, P_start, O_start]

			upper_bound = [n1_max, n2_max, m12_max, m21_max, Ts_max, Ti_max, P_max, O_max]
			lower_bound = [n1_min, n2_min, m12_min, m21_min, Ts_min, Ti_min, P_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)

		if done: print(("\n" + namemodel + " : done\n"))

	# Ancient Migration with heterogeneous Me (population-specific)
	# nu1, nu2, m12, m21, Ts, Ti, P1, P2, O
	if namemodel == "AM2M2P":
		func = dadi_models_2pop.AM2M2P

		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [n1_start, n2_start, m12_start, m21_start, Ts_start, Ti_start, P_start, P_start, O_start]

			upper_bound = [n1_max, n2_max, m12_max, m21_max, Ts_max, Ti_max, P_max, P_max, O_max]
			lower_bound = [n1_min, n2_min, m12_min, m21_min, Ts_min, Ti_min, P_min, P_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)

		if done: print(("\n" + namemodel + " : done\n"))

	# Ancient Migration with heterogeneous Ne (shared between populations) and heterogeneous Me (shared between populations): 
	# nu1, nu2, m12, m21, Ts, Ti, nr, bf, P, O
	if namemodel == "AM2N2M":
		func = dadi_models_2pop.AM2N2M

		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [n1_start, n2_start, m12_start, m21_start, Ts_start, Ti_start, nr_start, bf_start, P_start, O_start]
				
			upper_bound = [n1_max, n2_max, m12_max, m21_max, Ts_max, Ti_max, nr_max, bf_max, P_max, O_max]
			lower_bound = [n1_min, n2_min, m12_min, m21_min, Ts_min, Ti_min, nr_min, bf_min, P_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)

		if done: print(("\n" + namemodel + " : done\n"))

	# Ancient Migration with heterogeneous Ne (shared between populations) and heterogeneous Me (population-specific)
	# nu1, nu2, m12, m21, Ts, Ti, nr, bf, P1, P2, O
	if namemodel == "AM2N2M2P":
		func = dadi_models_2pop.AM2N2M2P

		for optimizationstate in opt_list:
			print(optimizationstate)

			if optimizationstate == "dual_anneal":
				params = [n1_start, n2_start, m12_start, m21_start, Ts_start, Ti_start, nr_start, bf_start, P_start, P_start, O_start]
		
			upper_bound = [n1_max, n2_max, m12_max, m21_max, Ts_max, Ti_max, nr_max, bf_max, P_max, P_max, O_max]
			lower_bound = [n1_min, n2_min, m12_min, m21_min, Ts_min, Ti_min, nr_min, bf_min, P_min, P_min, O_min]

			done, popt = callmodel(func=func, data=data, pts_l=pts_l, ns=ns, modeldemo=namemodel, namepop1=namepop1, namepop2=namepop2,
					 params=params, fixed_params=None, lower_bound=lower_bound, upper_bound=upper_bound,
					 maxiterGlobal=maxiterGlobal, accept=accept, visit=visit, Tini=Tini,
					 no_local_search=no_local_search, maxiterLocal=maxiterLocal, local_method=local_method, 
					 output_file=output_file, output_file_2=output_file_2, output_file_3=output_file_3, 
					 nameoutput=nameoutput + "/" + nameoutput, nameoutput2=nameoutput, verbose=verbose, full_output=True)

		if done: print(("\n" + namemodel + " : done\n"))

output_file.close()
output_file_2.close()
output_file_3.close()
